﻿
DeclareModule Action
  
  Enumeration
    #Tree
    #List
    #OnlyUnread
    #All
    #Next
    #Prev
    #Current
  EndEnumeration  
  
  Structure MsgIndex
    fake_id.l ; Номер сообщения в списке (на случай, если сообщения отображены деревом, а хочется получить порядковый номер, какой бы он был в списке) (для счётчиков и прочей дряни).
    index_id.l ; Порядковый номер сообщения в списке.
    tw_entry.i ; Ссылка на сообщение в списке.
    id.l ; ID сообщения в БД.
    area_type.l ; Тип арии.
    echoarea_id.l ; Если ария не нетмейл, то вот ID эхоконфы.
  EndStructure
  
  ; Временная индексация списка сообщений.
  Global NewList MsgsIndex.MsgIndex()
  
  ; Здесь будет собираться информация о создаваемом сообщении.
  Global reply.DB::Message
  Global msg.DB::Message

  Declare PasteAreasList()
  Declare OpenArea()
  Declare OpenCurrentMessage()
  Declare CreateMessage()
  Declare UnreadMessage()
  Declare MarkReadCurrentArea()
  Declare CreateReply()
  Declare DeleteMessage()
  Declare SwitchShowMessages()
  Declare SetActiveMsgInLG(msg_id)
  Declare SaveLastRead()
  Declare RefrashAreas()
  Declare SaveCurrentMessage()
  Declare RunScriptSendMail()
  
  Declare PreviousMessage()
  Declare NextMessage()
  
  ; Здесь будем хранить тип текущей арии.
  Global CurrentAreaType
  
   ; Здесь будем хранить название текущей арии.
  Global CurrentAreaName.s
  
  ; А здесь будем хранить echoarea_id, если в данный момент времени открыта эха.
  Global CurrentEchoAreaID
  
  ; А здесь будем хранить открытого в данный момент id сообщения.
  Global CurrentMsgID

EndDeclareModule

Module Action
  
  ;- Window_General
  ;{
    
  ; Используя DB::GetAreasList, отобразит арии в главном окне.
  Procedure PasteAreasList()
    lg_id = GUI::#GW_ListIcon_Areas
    If IsGadget(lg_id)
      ClearGadgetItems(lg_id)
      DB::GetAreasList(DB::AreasList.DB::Area(), DB::#All)
      SortStructuredList(DB::AreasList(), #PB_Sort_Descending, OffsetOf(DB::Area\unread), #PB_Integer)
      ForEach DB::AreasList()
        item.s = ""
        item.s + Str(CountGadgetItems(lg_id)+1)
        item.s + Chr(10)
        item.s + DB::AreasList()\name
        item.s + Chr(10)
        item.s + DB::AreasList()\unread
        item.s + Chr(10)
        item.s + DB::AreasList()\all
        AddGadgetItem(lg_id, num, item.s)
        SetGadgetItemData(lg_id, num, DB::AreasList()\fake_id)
        num + 1
      Next
    EndIf
  EndProcedure
  
  ; Выводит информацию об открытом сообщении.
  Procedure SetMsgInfo(*msg.DB::Message)
    SetGadgetText(GUI::#GW_MsgInfo_Msg, Str(*msg\fake_id) + " из " + Str( ListSize(MsgsIndex()) ))
    SetGadgetText(GUI::#GW_MsgInfo_From, *msg\from_name)
    SetGadgetText(GUI::#GW_MsgInfo_To, *msg\to_name)
    SetGadgetText(GUI::#GW_MsgInfo_Subj, *msg\subject)
    SetGadgetText(GUI::#GW_MsgInfo_From_FTN, *msg\from_ftn_addr)
    SetGadgetText(GUI::#GW_MsgInfo_To_FTN, *msg\to_ftn_addr)
    SetGadgetText(GUI::#GW_MsgInfo_DateReceipt, Date::CreateDateString(*msg\date))
  EndProcedure
  
  ;- Navigation
  ;{
  
  ; Используется для того, чтобы узнать ID сообщения, которое выбрано в списке сообщений 
  ; В случае неудачи вернёт -1.
  Procedure _cust_GetCurrentMsgIDFromMsgsList()
    Protected string_data.s, current_id
    string_data.s = TreeView::getCurrentItemDataStr(GUI::*GW_MessagesList)
    If string_data.s <> ""
      current_id = Val(string_data)
    Else
      current_id = -1
    EndIf
    ProcedureReturn current_id
  EndProcedure
  
  ; Используется для того, чтобы узнать index_id (порядковый номер в списке) сообщения.
  ; В случае неудачи вернёт -1.
  Procedure _cust_GetMsgIndexID(current_id)
    Protected index_id = -1
    ForEach MsgsIndex()
      If MsgsIndex()\id = current_id
        index_id = MsgsIndex()\index_id
        Break
      EndIf
    Next
    ProcedureReturn index_id
  EndProcedure
  
  ; Используется для того, чтобы узнать tw_entry (ссылку на пункт в TreeView) сообщения.
  ; В случае неудачи вернёт -1.
  Procedure.i _cust_GetMsgEntry(current_id)
    Protected tw_entry.i = -1
    ForEach MsgsIndex()
      If MsgsIndex()\id = current_id
        tw_entry = MsgsIndex()\tw_entry
        Break
      EndIf
    Next
    ProcedureReturn tw_entry
  EndProcedure

  ; Вернёт fake_id сообщения.
  ; В случае неудачи вернёт -1.
  Procedure _cust_GetMsgFakeID(current_id)
    Protected fake_id = -1
    ForEach MsgsIndex()
      If MsgsIndex()\id = current_id
        fake_id = MsgsIndex()\fake_id
        Break
      EndIf
    Next
    ProcedureReturn fake_id
  EndProcedure
  
  ; Вспомогалка для NextMessage() и PreviousMessage().
  ; Используется для того, чтобы выделить текущим следующее (если msg = #Next), или предыдущее (msg = #Prev) в списке сообщение.
  ; Вернёт msg_id выбранного сообщения в случае удачи, иначе - 0.
  Procedure _cust_SelectMsgFromMsgsList(msg)
    Protected index_id, current_id, tw_entry, msg_id
    index_id = -1
    tw_entry = -1
    current_id = _cust_GetCurrentMsgIDFromMsgsList()
    
    ; Узнать index_id нужного сообщения.
    index_id = _cust_GetMsgIndexID(current_id)

    If msg = #Prev
      index_id - 1
    ElseIf msg = #Next 
      index_id + 1
    EndIf
    
    ; Найти сообщение со значением index_id, получить значение tw_entry этого сообщения и его id.
    ForEach MsgsIndex()
      If MsgsIndex()\index_id = index_id
        tw_entry = MsgsIndex()\tw_entry
        msg_id = MsgsIndex()\id
        Break
      EndIf
    Next
    
    ; Отметить это сообщение текущим в TreeView.
    If tw_entry <> -1
      TreeView::setState(GUI::*GW_MessagesList, tw_entry)
    Else
      msg_id = -1
    EndIf
    
    ProcedureReturn msg_id
  EndProcedure
  
  ; Делает активным в списке сообщение msg_id.
  Procedure SetActiveMsgInLG(msg_id)
    TreeView::setState(GUI::*GW_MessagesList, _cust_GetMsgEntry(msg_id))
  EndProcedure
  
  ; Отметит текущее сообщение в списке прочитанным.
  Procedure MarkReadCurrentMsgFromLG()
    TreeView::setIcon(_cust_GetMsgEntry(_cust_GetCurrentMsgIDFromMsgsList()), 0, GUI::#GW_Image_MsgRead)
    TreeView::redraw(GUI::*GW_MessagesList)
  EndProcedure

  ; Отметит текущее сообщение в списке непрочитанным.
  Procedure MarkUneadCurrentMsgFromLG()
    TreeView::setIcon(_cust_GetMsgEntry(_cust_GetCurrentMsgIDFromMsgsList()), 0, GUI::#GW_Image_MsgUnread)
    TreeView::redraw(GUI::*GW_MessagesList)
  EndProcedure
  
  ; Отметит все сообщения прочитанным.
  Procedure MarkReadAllCurrentMsgs()
    TreeView::setIconAllItems(GUI::*GW_MessagesList, 0, GUI::#GW_Image_MsgRead)
  EndProcedure
  
  ; Отобразит сообщение msgid.
  Procedure OpenMessage(msg_id, area_type, echoarea_id=0)
    start_time = ElapsedMilliseconds()

    ; Для того, чтобы отобразить сообщение на необходимо:
    
    ; 1) Получить, собственно, само сообщение из БД.
    If area_type = DB::#Echomail
      result = DB::GetEchomailMsg(DB::msg.DB::Message, echoarea_id, msg_id)
    ElseIf area_type = DB::#Netmail
      result = DB::GetNetMailMsg(DB::msg.DB::Message, msg_id)
    EndIf
    DB::msg\fake_id = _cust_GetMsgFakeID(msg_id)
    
    If result <> 0 ; Усли сообщение удалось получить из БД.
      temp_time = ElapsedMilliseconds()
      
      ; 2) Отобразить информацию\текст сообщения.
      GUI::SetTextInWiewMessage(FTN::FixText(DB::msg\message))
      SetMsgInfo(DB::msg.DB::Message)
      
      time.f = (ElapsedMilliseconds()-temp_time)/1000
      ;Debug "Отобразил текст за "+time

      ; 3) Отметить это сообщение как прочитанное, если оно, конечно, уже не отмечено прочитанным! :Р
      temp_time = ElapsedMilliseconds()
      If DB::msg\read_msg = 0
        DB::MarkRead(area_type, msg_id, echoarea_id)
        MarkReadCurrentMsgFromLG()
        time.f = (ElapsedMilliseconds()-temp_time)/1000
        ;Debug "Отметил как прочитанное за "+time
      EndIf

      ; 4) И, напоследок, запомним информацию об открытом сообщении для остальных процедур.
      CurrentAreaType = area_type
      CurrentMsgID = msg_id
      CurrentEchoAreaID = echoarea_id

      time.f = (ElapsedMilliseconds()-start_time)/1000
      ;Debug "Отобразил сообщение за "+time
    EndIf
  EndProcedure
  
    ; Перейдёт на следующее в списке\дереве сообщение.
  Procedure NextMessage()
    Protected msg_id
    msg_id = _cust_SelectMsgFromMsgsList(#Next)
    If msg_id <> -1
      CurrentMsgID = msg_id
      OpenMessage(CurrentMsgID, CurrentAreaType, CurrentEchoAreaID)
    EndIf
  EndProcedure
  
  ; Перейдёт на предыдущее сообщение в списке\дереве.
  Procedure PreviousMessage()
    Protected msg_id
    msg_id = _cust_SelectMsgFromMsgsList(#Prev)
    If msg_id <> -1
      CurrentMsgID = msg_id
      OpenMessage(CurrentMsgID, CurrentAreaType, CurrentEchoAreaID)
    EndIf
  EndProcedure
  
  ;}
  
  ; Добавляет сообщение *msg.DB::Message в список с сообщениями GUI::*GW_MessagesList.
  ; Для того, чтобы результат стал виден на экране, не забудь применить
  ; TreeView::redraw(GUI::*GW_MessagesList).
  ; Возвращает ссылку на вставленный элемент (*result.TreeView::ct_entry).
  Procedure AddMsgInMsgsList(*msg.DB::Message, *parent.TreeView::ct_entry = TreeView::#parent_root)
    item.s + *msg\subject
    item.s + #TAB$
    item.s + *msg\from_name + " -> " + *msg\to_name
    item.s + #TAB$
    item.s + Date::CreateDateString(*msg\date)
    item.s + #TAB$
    item.s + *msg\fake_id
    
    If *msg\read_msg <> 1
      image_id = GUI::#GW_Image_MsgUnread
    Else
      image_id = GUI::#GW_Image_MsgRead
    EndIf
    
    result = TreeView::addItem(GUI::*GW_MessagesList, *parent, item.s, image_id, Str(*msg\id))
    
    AddElement(MsgsIndex())
    With MsgsIndex()
      \fake_id = *msg\fake_id
      \index_id = *msg\index_id
      \tw_entry = result
      \id = *msg\id
      \area_type = *msg\area_type
      \echoarea_id = *msg\echoarea_id
    EndWith
    
    ProcedureReturn result
  EndProcedure
  
  ; Вспомогательная процедура для PasteMsgsTree().
  Procedure ParseMsgsTree(lg_id, *Tree.DB::Tree, level, lastread, *root.TreeView::ct_entry = TreeView::#parent_root, num = 0)
    msg.DB::Message
    ForEach *Tree\Reply()
      SelectElement(DB::MsgsList(), *Tree\Reply()\fake_id)

      With msg
        \index_id = num
        \id = DB::MsgsList()\id
        \subject = DB::MsgsList()\subject
        \from_name = DB::MsgsList()\from_name
        \to_name = DB::MsgsList()\to_name
        \date = DB::MsgsList()\date
        \read_msg = DB::MsgsList()\read_msg
        \fake_id = DB::MsgsList()\fake_id + 1
      EndWith

      *child = AddMsgInMsgsList(msg, *root)
      DB::MsgsList()\tw_entry = *child
      num + 1

      If lastread_id = DB::MsgsList()\id
        TreeView::setState(GUI::*GW_MessagesList, *child)
      EndIf
      
      If ListSize(*Tree\Reply()) <> 0
        num = ParseMsgsTree(lg_id, *Tree\Reply(), level+1, lastread, *child, num)
      EndIf
    Next
    ProcedureReturn num
  EndProcedure
  
  ; Отобразит дерево сообщений из MsgsList.Message() и Tree.Tree в главном окне.
  Procedure PasteMsgsTree(lastread)
    Protected start_time, time.f, Tree.DB::Tree
    start_time = ElapsedMilliseconds()
    DB::CreateMsgsTree(DB::MsgsList.DB::Message(), Tree)
    TreeView::clearAllItems(GUI::*GW_MessagesList)
    ParseMsgsTree(lg_id, Tree, 0, lastread)
    SortStructuredList(DB::MsgsList(), #PB_Sort_Ascending, OffsetOf(DB::Message\fake_id), TypeOf(DB::Message\fake_id))
    ClearList(Tree\Reply())
    ClearList(DB::MsgsList())
    SetActiveMsgInLG(lastread)
    OpenMessage(lastread, CurrentAreaType, CurrentEchoAreaID)
    TreeView::redraw(GUI::*GW_MessagesList)
    time.f = (ElapsedMilliseconds()-start_time)/1000
    Debug "На его отрисовку потребовалось "+time
  EndProcedure
  
  ; Отобразит сообщения из MsgsList.Message() в главном окне. Установит активным сообщение lastread_id.
  Procedure PasteMsgsList(lastread_id)
    Protected start_time, time.f, msg.DB::Message, *child
    start_time = ElapsedMilliseconds()
    TreeView::clearAllItems(GUI::*GW_MessagesList)
    
    ForEach DB::MsgsList.DB::Message()
      DB::MsgsList()\fake_id = num
      msg\id = DB::MsgsList()\id
      msg\fake_id = DB::MsgsList()\fake_id + 1
      msg\index_id = msg\fake_id ; В режиме отображения списка сообщений как лист эти поля совпадают.
      msg\subject = DB::MsgsList()\subject
      msg\from_name = DB::MsgsList()\from_name
      msg\to_name = DB::MsgsList()\to_name
      msg\date = DB::MsgsList()\date
      msg\read_msg = DB::MsgsList()\read_msg

      *child = AddMsgInMsgsList(msg)
      DB::MsgsList()\tw_entry = *child
      
      If lastread_id = DB::MsgsList()\id
        TreeView::setState(GUI::*GW_MessagesList, *child)
      EndIf

      num + 1
    Next

    ClearList(DB::MsgsList())
    SetActiveMsgInLG(lastread_id)
    OpenMessage(lastread_id, CurrentAreaType, CurrentEchoAreaID)
    TreeView::redraw(GUI::*GW_MessagesList)
    time.f = (ElapsedMilliseconds()-start_time)/1000
    Debug "Отобразил список из "+num+" сообщений за "+time
  EndProcedure
  
  ; Используется при закрытии приложения, а также в OpenArea() чтобы при переходе на другую арию 
  ; запомнить последнее прочитанное сообщение в предыдущей.
  Procedure SaveLastRead()
    If CurrentAreaType <> 0 And CurrentMsgID <> 0
      DB::SetLastreadMsg(CurrentMsgID, CurrentAreaType, CurrentEchoAreaID)
    EndIf
  EndProcedure  
  
  ; Отобразит сообщения выбранной арии.
  Procedure OpenArea()
    SaveLastRead()
    ClearList(MsgsIndex())
    ; Узнаём, какая именно ария выбрана в гл. окне.
    lg_item_state = GetGadgetState(GUI::#GW_ListIcon_Areas)
    If lg_item_state <> -1
      fake_id = GetGadgetItemData(GUI::#GW_ListIcon_Areas, lg_item_state)
      ForEach DB::AreasList()
        If DB::AreasList()\fake_id = fake_id
          CurrentAreaName = DB::AreasList()\name
          CurrentAreaType = DB::AreasList()\area_type
          CurrentEchoAreaID = DB::AreasList()\echoarea_id
          Break
        EndIf
      Next
      ; Если это эхоконференция, то..
      If CurrentAreaType = DB::#Echomail
        DB::GetEchomailList(DB::MsgsList.DB::Message(), CurrentEchoAreaID, GUI::Window\show_only_unread)
      ElseIf CurrentAreaType = DB::#Netmail ; Если нетмейл...
        DB::GetNetMailList(DB::MsgsList.DB::Message(), GUI::Window\show_only_unread)
      EndIf
      ; И не забываем получить id последнего непрочитанного сообщения в данной арии.
      lastread = DB::GetLastreadMsg(CurrentAreaType, CurrentEchoAreaID)
      If GUI::Window\msgs_mode = GUI::#List
        PasteMsgsList(lastread)
      ElseIf GUI::Window\msgs_mode = GUI::#Tree
        PasteMsgsTree(lastread)
      EndIf
    EndIf
  EndProcedure
  
  ; Отобразит выбранное в списке сообщений письмо.
  Procedure OpenCurrentMessage()
    new_msg_id = Val(TreeView::getCurrentItemDataStr(GUI::*GW_MessagesList))
    If new_msg_id <> -1 And new_msg_id <> CurrentMsgID
      CurrentMsgID = new_msg_id
      OpenMessage(CurrentMsgID, CurrentAreaType, CurrentEchoAreaID)
    EndIf
  EndProcedure
  
  ; Вернёт название, открытой в данный момент времени, арии.
  Procedure.s GetNameCurrentArea()
    lg_id = GUI::#GW_ListIcon_Areas
    ProcedureReturn GetGadgetItemText(lg_id, GetGadgetState(lg_id), 1)
  EndProcedure  
  
  ; Отметить текущее сообщение не прочитанным.
  Procedure UnreadMessage()
    DB::MarkUnread(CurrentAreaType, CurrentMsgID, CurrentEchoAreaID)
    MarkUneadCurrentMsgFromLG()
  EndProcedure
  
  ; Отметит все сообщения в текущей области прочитанным.
  Procedure MarkReadCurrentArea()
    DB::MarkAllRead(CurrentAreaType, CurrentEchoAreaID)
    MarkReadAllCurrentMsgs()
  EndProcedure  
    
  ; Удалит текущее сообщение.
  Procedure DeleteMessage()
    
    Protected info_msg.DB::Message, info.s, result

    If CurrentAreaType = DB::#Echomail
      DB::GetEchomailMsg(info_msg.DB::Message, CurrentEchoAreaID, CurrentMsgID)
    ElseIf CurrentAreaType = DB::#Netmail
      DB::GetNetMailMsg(info_msg.DB::Message, CurrentMsgID)
    EndIf
    
    info.s = "Вы действительно хотите удалить это сообщение?"+Chr(10)
    info.s + "#"+Str(_cust_GetMsgFakeID(info_msg\id))+Chr(10)
    info.s + info_msg\subject+Chr(10)
    info.s + info_msg\from_name+"->"+info_msg\to_name+Chr(10)
    
    result = MessageRequester("", info.s, #PB_MessageRequester_YesNo)
    If result = #PB_MessageRequester_Yes
      DB::DeleteMessage(CurrentAreaType, CurrentMsgID, CurrentEchoAreaID)
      OpenArea()
    EndIf
  
  EndProcedure
  
  ; Переключает тип отображения панели сообщений между отображением всех и только непрочитанных.
  Procedure SwitchShowMessages()
    If GUI::Window\show_only_unread = 1
      GUI::Window\show_only_unread = 0
      SetMenuItemState(GUI::#GW_MenuBar, GUI::#GW_Fun_UI_ShowOnlyUnread, 0)
      OpenArea()
    Else
      GUI::Window\show_only_unread = 1
      SetMenuItemState(GUI::#GW_MenuBar, GUI::#GW_Fun_UI_ShowOnlyUnread, 1)
      OpenArea()
    EndIf
  EndProcedure
  
  ; Обновит информацию в списке арий.
  Procedure RefrashAreas()
    Action::PasteAreasList()
  EndProcedure
  
  ; Позволяет сохранить текущее сообщение в файл.
  Procedure SaveCurrentMessage()
    Protected *msg.DB::Message, text.s
    
    ; Получаем сообщение из БД.
    If CurrentAreaType = DB::#Echomail
      result = DB::GetEchomailMsg(msg.DB::Message, CurrentEchoAreaID, CurrentMsgID)
    ElseIf CurrentAreaType = DB::#Netmail
      result = DB::GetNetMailMsg(msg.DB::Message, CurrentMsgID)
    EndIf
    msg\fake_id = _cust_GetMsgFakeID(msg\id)
    
    ; Усли сообщение удалось получить из БД, записываем его в файл.
    If result <> 0
      ; Узнаём у пользователя - куда нужно сохранить сообщение.
      title.s = "Сохранить сообщение в файл:"
      
      filename.s = CurrentAreaName
      filename.s + " - "
      ;filename.s + msg\subject
      ;filename.s + " - "
      filename.s + msg\fake_id
      filename.s + ".msg"
      
      path.s = SaveFileRequester(title.s, filename.s, "msg (*.msg)|All files (*.*)|*.*", 0)
      
      ; Создаём файл для сообщения.
      If path.s <> ""
        If CreateFile(0, path.s, #PB_UTF8)
          text.s = "Область: "+CurrentAreaName
          text.s + Chr(10)
          text.s + "Сообщение: "+msg\fake_id+" из "+Str(ListSize(MsgsIndex()))+"   ("+Date::CreateDateString(msg\date)+") "
          text.s + Chr(10)
          text.s + "От:   "+msg\from_name+"   "+msg\from_ftn_addr
          text.s + Chr(10)
          text.s + "Кому: "+msg\to_name+"   "+msg\to_ftn_addr
          text.s + Chr(10)
          text.s + "Тема: "+msg\subject
          text.s + Chr(10)
          text.s + msg\message
  
          WriteString(0, text.s, #PB_UTF8)
          CloseFile(0)
        Else
          MessageRequester("Dozens","Не удалось создать сообщение, ошибка при создании файла.")
        EndIf
      EndIf
    Else
      MessageRequester("Dozens","Не удалось создать сообщение, ошибка при получении из БД.")
    EndIf
  EndProcedure
  
  ; Запускает скрипт отправки почты (запуска jNode).
  Procedure RunScriptSendMail()
    RunProgram(CFG::jNodeScriptSend.s, "", GetPathPart(CFG::jNodeScriptSend.s))
  EndProcedure
  
  ;}
  
  ;- Window_Editor
  ;{
  
  ; Заполнит гаджеты окна #Window_Editor информацией из msg.DB::Message.
  Procedure SetEditorWindowData(msg_type, *msg.DB::Message)
    window_name.s = GetWindowTitle(GUI::#Window_Editor)
    area_name.s = GetNameCurrentArea()
    SetWindowTitle(GUI::#Window_Editor, window_name.s+" - "+area_name.s)
    SetGadgetText(GUI::#EW_String_From, *msg\from_name)
    SetGadgetText(GUI::#EW_String_From_Ftn, *msg\from_ftn_addr)
    SetGadgetText(GUI::#EW_String_To, *msg\to_name)
    GUI::SetTextInEditorMessage(*msg\message)
    If msg_type = FTN::#Reaply
      SetGadgetText(GUI::#EW_String_To_Ftn, *msg\to_ftn_addr)
      SetGadgetText(GUI::#EW_String_Subj, *msg\subject)
    EndIf
  EndProcedure
  
  ; Вернёт в msg.DB::Message информацию из гаджетов окна #Window_Editor.
  Procedure GetEditorWindowData(*msg.DB::Message)
    *msg\from_name      = GetGadgetText(GUI::#EW_String_From)
    *msg\from_ftn_addr  = GetGadgetText(GUI::#EW_String_From_Ftn)
    *msg\to_name        = GetGadgetText(GUI::#EW_String_To)
    *msg\to_ftn_addr    = GetGadgetText(GUI::#EW_String_To_Ftn)
    *msg\subject        = GetGadgetText(GUI::#EW_String_Subj)
    *msg\message        = ReplaceString(GUI::GetTextFromEditorMsg(), Chr(13)+Chr(10), Chr(13)) ; Заменяем CRLF на CR.
  EndProcedure

  ; Закроет окно создания нового сообщения в текущей арии.
  Procedure CloseWindowEditor()
    ClearStructure(@reply, DB::Message)
    ClearStructure(@msg, DB::Message)
    CloseWindow(GUI::#Window_Editor)
    GUI::UnlockCreateMsgFunctions()
  EndProcedure
  
  ; Создаёт новое сообщение из msg.DB::Message и информации из окна #Window_Editor.
  Procedure SaveMessage()
    ; Получаем информацию о письме из окна редактора.
    GetEditorWindowData(msg.DB::Message)
    ; Запоминаем текущее время в формате UTC.
    msg\date = Date::UTCTime()*1000
    ; Формируем общие кладжи.
    kludges.s = Kludges::GetPidKludge()
    kludges.s + Kludges::GetChrsKludge()
    kludges.s + Kludges::GetTzutcKludge()
    If msg\area_type = DB::#Netmail
      kludges.s = Kludges::CreateMsgidKludge(msg\from_ftn_addr) + kludges.s
      msg\message = kludges.s + msg\message
      DB::CreateNetmailMsg(msg.DB::Message)
    ElseIf msg\area_type = DB::#Echomail
      msg\message = kludges.s + msg\message
      msg\msgid = Kludges::CreateMsgidString(msg\from_ftn_addr) ; В отличии от нетмыла, здесь для кладжа msgid есть отдельный столбец в таблице.
      DB::CreateEchomailMsg(msg.DB::Message)
    EndIf
    CloseWindow(GUI::#Window_Editor)
    GUI::UnlockCreateMsgFunctions()
    ClearStructure(@msg, DB::Message)
  EndProcedure
  
  ; Создаёт ответ из reply.DB::Message из информации из окна #Window_Editor.
  Procedure SaveReply()
    GetEditorWindowData(reply.DB::Message)
    reply\date = Date::UTCTime()*1000
    ; Формируем общие кладжи.
    kludges.s = Kludges::GetPidKludge()
    kludges.s + Kludges::GetChrsKludge()
    kludges.s + Kludges::GetTzutcKludge()
    ;- Кстати, а у нас msg затирается от сообщения к сообщению?
    If reply\reply<>""
      kludges.s = Kludges::CreateReplyKludge(reply\reply) + kludges.s
    EndIf
    If reply\area_type = DB::#Netmail
      kludges.s = Kludges::CreateMsgidKludge(reply\from_ftn_addr) + kludges.s
      reply\message = kludges.s + reply\message
      DB::CreateNetmailMsg(reply.DB::Message)
    ElseIf reply\area_type = DB::#Echomail
      reply\message = kludges.s + reply\message
      reply\msgid = Kludges::CreateMsgidString(reply\from_ftn_addr) ; В отличии от нетмыла, здесь для кладжа msgid есть отдельный столбец в таблице.
      DB::CreateEchomailMsg(reply.DB::Message)
    EndIf
    CloseWindow(GUI::#Window_Editor)
    GUI::UnlockCreateMsgFunctions()
    ClearStructure(@reply, DB::Message)
    ClearStructure(@msg, DB::Message)
  EndProcedure
  
  ; Откроет окно создания нового сообщения в текущей арии.
  Procedure CreateMessage()
    msg\area_type = CurrentAreaType
    msg\from_name = CFG::User\Name
    msg\from_ftn_addr = CFG::User\Address
    ; В какой арии будем создавать сообщение?
    If CurrentAreaType = DB::#Echomail
      msg\echoarea_id = CurrentEchoAreaID
      msg\to_name = "All"
    EndIf
    temp.FTN::TplVariables
    temp\from_name = msg\from_name
    temp\to_name = msg\to_name
    msg\message = FTN::LoadTemplate(temp, "new")+FTN::GetSignatures()
    ; Отображаем окно.
    GUI::LockCreateMsgFunctions()
    GUI::OpenWindow_Editor()
    SetEditorWindowData(FTN::#New, msg.DB::Message)
    BindGadgetEvent(GUI::#EW_Button_Cancel, @CloseWindowEditor())
    BindGadgetEvent(GUI::#EW_Button_OK, @SaveMessage())
  EndProcedure
  
  ; Откроет окно создания ответа на выбранное сообщение в текущей арии.
  Procedure CreateReply()

    ;{ Запоминаем информацию, необходимую для создания письма.
    
    reply\area_type = CurrentAreaType
    reply\from_name = CFG::User\Name
    reply\from_ftn_addr = CFG::User\Address
    ; Получаем информацию о письме, на которое мы отвечаем.
    If CurrentAreaType = DB::#Netmail
      DB::GetNetMailMsg(msg, CurrentMsgID)
      reply\to_ftn_addr = msg\from_ftn_addr
    ElseIf CurrentAreaType = DB::#Echomail
      reply\echoarea_id = CurrentEchoAreaID
      DB::GetEchomailMsg(msg, CurrentEchoAreaID, CurrentMsgID)
    EndIf
    reply\subject = msg\subject
    ; Если в начале строки сабжа нет 'Re: ', то добавляем её.
    If CountString(UCase(Left(reply\subject, 5)), "RE:") = 0
      reply\subject   = "Re: "+reply\subject
    EndIf
    reply\reply     = msg\msgid
    reply\to_name   = msg\from_name
    temp.FTN::TplVariables
    temp\from_name = reply\from_name
    temp\to_name = reply\to_name
    temp\from_ftn_addr = msg\from_ftn_addr
    temp\reply_to_name = msg\to_name
    temp\reply_to_ftn_addr = msg\to_ftn_addr
    temp\reply_date = msg\date
    temp\quoted_text = msg\message
    reply\message = FTN::LoadTemplate(temp, "reply")+FTN::GetSignatures()

    ;}
    
    ; Отображаем окно.
    GUI::LockCreateMsgFunctions()
    GUI::OpenWindow_Editor()
    SetEditorWindowData(FTN::#Reaply, reply.DB::Message)
    BindGadgetEvent(GUI::#EW_Button_Cancel, @CloseWindowEditor())
    BindGadgetEvent(GUI::#EW_Button_OK, @SaveReply())
  EndProcedure

  ;}
  
EndModule
; IDE Options = PureBasic 5.31 (Windows - x86)
; CursorPosition = 232
; FirstLine = 119
; Folding = 4gYt50--
; EnableUnicode
; EnableXP