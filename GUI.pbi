﻿;- "CustomTreeViewGadget.pbi"
XIncludeFile "CustomTreeViewGadget.pbi"

DeclareModule GUI
  
  Enumeration
    #Window_General
    #Window_Editor

    #GW_ListIcon_Messages
    #GW_ListIcon_TreeMsgs
    #GW_ListIcon_Areas
    #GW_Scintilla_WiewMessage
    #GW_Image_MsgRead
    #GW_Image_MsgUnread
    
    #GW_MenuBar
    #GW_Fun_Temp ;- #GW_Fun_Temp
    #GW_Fun_PrevMsg
    #GW_Fun_NextMsg
    #GW_Fun_Exit
    #GW_Fun_CreateMsg
    #GW_Fun_ReplyMsg
    #GW_Fun_Msg_Delete
    #GW_Fun_Msg_Unread
    #GW_Fun_MarkReadCurrentArea
    #GW_Fun_Setings
    #GW_Fun_About
    #GW_Fun_MsgList
    #GW_Fun_RefrashAreas
    #GW_Fun_UI_Classic
    #GW_Fun_UI_ShowOnlyUnread
    #GW_Fun_UI_Vertical
    #GW_Fun_UI_MsgsWiew_List
    #GW_Fun_UI_MsgsWiew_Tree
    #GW_Fun_SaveMsgInFile
    #GW_Fun_Run_SendMail
    
    #EW_String_From
    #EW_String_To
    #EW_String_From_Ftn
    #EW_String_Subj
    #EW_String_To_Ftn
    #EW_Editor_Msg
    #EW_Button_OK
    #EW_Button_Cancel

    #GW_Container_Areas
    #GW_Container_Messages
    #GW_Splitter_ListAndWiew_V
    #GW_Splitter_ListAndWiew_H
    #GW_Container_WiewMsg
    #GW_Container_ListAndWiew
    #GW_Splitter_AreasAndMsgs
    
    #GW_MsgInfo_N_Msg
    #GW_MsgInfo_N_From
    #GW_MsgInfo_N_To
    #GW_MsgInfo_N_Subj
    #GW_MsgInfo_Atr
    #GW_MsgInfo_From_FTN
    #GW_MsgInfo_Subj
    #GW_MsgInfo_Msg
    #GW_MsgInfo_From
    #GW_MsgInfo_To
    #GW_MsgInfo_To_FTN
    #GW_MsgInfo_DateReceipt
    
    #GW_Toolbar
    #Img_PreviousMsg
    #Img_NextMsg
    #Img_RefrashAreas
    #Img_NewMsg
    #Img_SendMail
    #Img_ReplyMsg
  EndEnumeration
  
  Enumeration UI
    #Classic
    #Vertical
    #List
    #Tree
  EndEnumeration
  
  Declare OpenWindow_General()
  Declare OpenWindow_Editor()
  Declare SetTextInWiewMessage(text.s)
  Declare SetTextInEditorMessage(text.s)
  Declare.s GetTextFromEditorMsg()
  Declare LockCreateMsgFunctions()
  Declare UnlockCreateMsgFunctions()
  Declare SwitchTypeUI(type)
  Declare SwitchMsgsViewType(type)
  
  Structure UI
    x.l
    y.l
    h.l
    w.l
    state.l
    
    type.l ; Тип расположения гаджетов в #GW_Container_ListAndWiew.
    msgs_mode.l ; Тип отображения панели сообщений.
    show_only_unread.b ; Равно 1, если включено отображение только непрочитанных сообщений.
    
    splitter1.l
    splitter2.l
  EndStructure
  
  ; Здесь будем хранить габариты окна.
  Global Window.UI
  
  ;- #GW_ListIcon_TreeMsgs Взаимодействие с деревом сообщений через здесь блиа
  Global *GW_MessagesList.TreeView::ct_main
  
EndDeclareModule

Module GUI
  
  ;- TEMP IMAGE
  ;{ 
  
  DataSection
  folderyellow_png:
    Data.q $0A1A0A0D474E5089,$524448490D000000,$3000000030000000,$F902570000000608,$4752730100000087
    Data.q $0000E91CCEAE0042,$FF0044474B620600,$93A7BDA0FF00FF00,$7359487009000000,$C3140000C3140000
    Data.q $000000424D701501,$04DB07454D497407,$E5DE7EDB000B0B16,$54414449B8070000,$575C8F4999EDDE68
    Data.q $3ABAA1BEE77FC715,$445890B1831019C4,$04A26C3624604688,$43E4B0592C483163,$C22241B22C160904
    Data.q $31002C166B023E7B,$98C0885020589158,$50F6EC83B638A090,$78B0F7BA6F0D7774,$AA2C11B9EA77BEF7
    Data.q $7ABD5552A4FC2465,$3C14F73FFCCF7FF7,$F57D52F78F5C9EB9,$FD7D7F0FA79FEEB1,$DBC680AFE738AFCB
    Data.q $FFD0501DAFE24401,$BECC5E4273210016,$4902A71FF7EAF227,$2F67E82F93FF7E42,$F01E0D0EFF5F077F
    Data.q $51D414E0668F3380,$BF7FBB88E0F961CE,$3C27FE46FD2EA7CE,$93EA55C5EA4B3610,$7756887C28B880BF
    Data.q $3F98CCD6501CA151,$2F57EB7D5B4419F3,$D01DF1273E2C93AA,$184C1E8DB40459F8,$B8DD579367C6500D
    Data.q $4AB3E2F1BD0AEB76,$1BFE7CAF8B172D59,$9B0D9B00A5BEBC6F,$885C424146F87BC5,$AAE86D01AC3924D9
    Data.q $C5908B8FE1FB3C6B,$17DC317824A369FB,$183358651033F749,$E421C541442C634A,$45B5411455165108
    Data.q $37873F3BD5F3315C,$AC9CD0DA640A5BE6,$4AAD47B99B89BE08,$25B543200BA2BD18,$66FD742103E08C84
    Data.q $0EA02A0A2126BD1A,$CAF48A032C219068,$0057B45497A22DD8,$CB9CA28444798A8D,$A1C84247BDCB5B83
    Data.q $090C2409F51AAB34,$0777479931E7BFDB,$D54D15EBA8EF04F5,$CDBD08077C1034D2,$BB4B26028D1485E8
    Data.q $0292A342C5338BF8,$C511154045F7A3A2,$7BEDE372DFB7B089,$A8371CD2A880E68F,$626E80B4610A137A
    Data.q $AAA841085704C458,$47158B4735272735,$7D43AA094E842747,$CBEF199C9788BA10,$B6EAD170404A1EA0
    Data.q $FD8CB2C94851A8BE,$42A94EAEEF9B92FD,$8BA50C7F9288FA3F,$AC667CB459B0930B,$E1AAF59EF3A1401F
    Data.q $072AFE5E2E13FDDE,$9D0A53E9F4097E2F,$876114EBF2674E4D,$21684803BC37FBBC,$F06D7ADF7395DF54
    Data.q $B8C49D5E3969BD40,$C3709460D0156E4B,$59E37AC77DE28A86,$1D53AFA5E0E23FDC,$C72D98227AD119FE
    Data.q $7EF1F21549DA4F89,$964B40218B614538,$BC007E776FF373EF,$BE5E0E6DFABF5F96,$12C7FCDC87C3C3C2
    Data.q $BA49BD8231710047,$66C95411C1A6D2ED,$4B5668D7E2FC5C7B,$05128C9332541B54,$AC1A5FBC59F85175
    Data.q $863A73EC9B36D8C2,$381CFB0EB7BB9E73,$DB95ECFE6F0AF978,$BA72B5DAA4BD2F17,$5DA1A5D464754F4B
    Data.q $8F45D446DEAFC03F,$091F3C5A3E39B231,$72A24DFC65102871,$36723863A703C4F5,$2EF7C3E3E24C6EEB
    Data.q $53AD5AD74B7FF774,$64C2F592FB8CE9D5,$572D63F14A4FAC93,$0A9388D2D246520F,$3EC30FC0C3843143
    Data.q $51DB2D59444319FF,$020C239D41B2F581,$4C6A564AF333C110,$2B1EAD62DC91874C,$60E133A8B0DAC007
    Data.q $99431AFFBAC19231,$20BAF7A0547E75D1,$6571A99B33B00C1A,$FCA2DD0DE26E810A,$1A68D0F5A0F0C3B3
    Data.q $DC6B42B58D71F01E,$8AC98B293157932E,$0C85E48C0B3C371A,$690494A8A2A028C1,$A802D005CBA30023
    Data.q $011AB022F607A80C,$A30CEE828C37934B,$9B192E4424A5570F,$6A9903D31A443A39,$F1376629D3A99D22
    Data.q $6AD63061187844D9,$24458C888B118876,$9D9F6D8F7DB41F1F,$72E26168C4CEC989,$BA312EF6EA6A3E4A
    Data.q $D0769D88842B6301,$6C95A2EA37526554,$08CAAF8F2B58C015,$99AA18367B313339,$3CA6D61170860F8D
    Data.q $3BA365826C589116,$84CDA5FA4F51CA87,$56C609B2308EFCEC,$0128433323CAC40E,$309BDF789DA74FD8
    Data.q $62CB607C332E65B0,$2275FBAC615D8EE2,$F04215501AA039DB,$6F34951C634F99DB,$E16393F3D25AAB0D
    Data.q $D68F436754386159,$3A6E488B481C407B,$253380E69655D4C3,$927E721373DE609B,$B067608D48C0BBA0
    Data.q $8EBF7EDA82A94652,$6CBCB498F253738B,$EF5C4CE0BB76CB11,$E2DB10868C1CC30A,$D2C5AD8B33B40C81
    Data.q $53E83ED955899563,$118577D184DD611A,$2751440784C87F0C,$3221958E4CD1D0E6,$6C20D6C342AA6367
    Data.q $6D558CC1186EDD88,$F56D4F222CF11817,$A67B2BDF238564C2,$28DAA2553E686713,$6656AA4FEE971855
    Data.q $C798C0BBDEB6C708,$3C665C1BCC52ACA3,$FBE1E9F8378FE370,$60D3BB4CA51D3F78,$F56D9FB5EE30A848
    Data.q $F77833C8E2863DD8,$C73FCEE78151BDFA,$B329DCEF873B9F0B,$32CD9A9E6194EED0,$AF46059D6E1D9823
    Data.q $3CB6B3BDDC665E50,$AF71A80F23EF04A0,$0A89CCA9564D589A,$57CD4D000118B0A3,$681CC042F04639D8
    Data.q $72756BA3AEF8F782,$6D6243484FB628F0,$D872A6B4386DDB8C,$1D1C257A30AECB03,$734FC9FF3E2619DA
    Data.q $948D37AD7413F970,$C2FEEA6B58D6A84D,$76EF9F6CD793B81C,$BFBC66516287D188,$787CB02BD5BC06B8
    Data.q $849B5EA6867AAE9B,$62ABF23F6C6186C9,$F1ED2669C213F025,$4B8488C2BB4E4266,$F47B707939A7F883
    Data.q $EAD27CE4538D3880,$06126A99E49478C6,$77DF3333E44C33B6,$5EFF86A93B1B118D,$D306825B415894F2
    Data.q $249CB03EDDE3A6BE,$3B57899D91693162,$CD040625DD164B4D,$B40D34038929ACA2,$78C561699D0EED8D
    Data.q $DD22E9328B293666,$D6F8A9FBC7A46219,$7F14BA1152736558,$2701EFB3DB112185,$D70D4F3663611193
    Data.q $10E28619D96FD4DA,$2E8CA6738E7A84F1,$9C17F9CD64EF168E,$55E08BDE2E24E00F,$CB5836D8DC4506AC
    Data.q $5853CEC66B88BDF8,$D8E94890FB532145,$0B53F78A3814BE9C,$8C24328CB58A9B9F,$FAC26C16E90F651A
    Data.q $B801730FC5FEBF0F,$96DA9739966F9174,$63F68845FF8B8219,$FEF78657A4FDA730,$BFA3FA8D3EF81BB3
    Data.q $FDD9869FF87173D3,$40545145DBFC6704,$19FE9393BE915055,$BCBB12A0E77D2080,$7F572F5BA9CCB6B4
    Data.q $46BD42A078FEA6F9,$906F1C29504FE0F6,$1EDEC1915F0BEB3C,$7F6566463DDC1994,$EB7B967ECFFFB872
    Data.q $63FDC35AA42B2C0E,$A1AAA405D211CEE4,$959D0C8A26BFFA6E,$907C2E0CFEE8E650,$47C2E0CB2007ECF0
    Data.q $8E7CC1DC829FE7D1,$5D0665A25FB22DD6,$9B9C8F18C78AAA80,$C45C801FCF0773A3,$FAE4FF51FC9E87C3
    Data.q $78C25AC505FEBFBF,$00000000F7457727,$826042AE444E4549
EndDataSection
  
  ;}

  LoadImage(#GW_Image_MsgRead, "icons\read.png")
  LoadImage(#GW_Image_MsgUnread, "icons\unread.png")
  
  ;}
  
  InitScintilla()
  
  ; Изменяет текст в scintill'е.
  Procedure MakeScintillaText(text.s)
    Static sciText.s
    CompilerIf #PB_Compiler_Unicode
      sciText = Space(StringByteLength(text, #PB_UTF8))
      PokeS(@sciText, text, -1, #PB_UTF8)
    CompilerElse
      sciText = text
    CompilerEndIf
    ProcedureReturn @sciText
  EndProcedure
  
  ;- [СТИЛЕВЫДЕЛЕНИЕ] Callback.
  Procedure SCI_Callback(gadget, *scinotify.SCNotification)
    ;- Так ведь не keyword.s, а строка же.
    Protected state, old_state, current_position, end_position, end_styled_position, 
              linenumber, char, keyword.s, keyword_start_position
    ; Чтобы не городить обращения к структуре вида *scinotify.SCNotification\nmhdr\code, 
    ; а просто писать \nmhdr\code, будем использовать With:EndWith.
    With *scinotify.SCNotification
      Select \nmhdr\code ; Через данную переменную можно отлавливать события вида SCN_*.
        Case #SCN_STYLENEEDED ; Текст должен подвергнуться стилистической правке.
          end_position = \Position
          ; Узнаём до какой позиции следует подсвечивать синтаксис в редакторе.
          end_styled_position = ScintillaSendMessage(gadget, #SCI_GETENDSTYLED)
          ; Узнаём номер строки, до которой следует подсвечивать синтаксис.
          linenumber = ScintillaSendMessage(gadget, #SCI_LINEFROMPOSITION, end_styled_position)
          ; Узнаём позицию первого символа в строке, заданной в "linenumber".
          current_position = ScintillaSendMessage(gadget, #SCI_POSITIONFROMLINE, linenumber)
          ; Подготовка к стилистической правке.
          ScintillaSendMessage(gadget, #SCI_STARTSTYLING, current_position, $1F | #INDICS_MASK)
          ;- Ещё что-то...
          state = Styling::#LexerState_Space 
          keyword_start_position = current_position
          keyword.s = "" 
          
          ; Поехали перебирать символы!
          While current_position <= end_position
            old_state = state ;- Как поймёшь что это - напишешь.
            
            char = ScintillaSendMessage(gadget, #SCI_GETCHARAT, current_position) ; Получаем символ из текущей позиции курсора.
            
            If Char = 10 Or Char = 13 ; Отмечаем символы "возврата каретки" и "перевода строки" как пробелы.
              state = Styling::#LexerState_Space 
            Else
              state = Styling::#LexerState_Keyword
              keyword + Chr(char) ; В переменной "keyword" сохраняется текст для поиска ключевых слов
            EndIf
          
            If old_state <> state Or current_position = end_position
              If old_state = Styling::#LexerState_Keyword 
                ;- Здесь можно сравнивать слова с чем либо...
                old_state = Styling::GetStyle(keyword.s, old_state)
                keyword.s = "" 
              EndIf
              ; Собственно, стилистическая правка.
              ScintillaSendMessage(gadget, #SCI_SETSTYLING, current_position - keyword_start_position, old_state)
              keyword_start_position = current_position 
            EndIf
            
            current_position + 1
          Wend
      EndSelect
    EndWith
  EndProcedure
  
  ;- [СТИЛЕВЫДЕЛЕНИЕ] SetScintillaStyle.
  Procedure SetScintillaStyle(gadget_id)
    ScintillaSendMessage(gadget_id, #SCI_SETCODEPAGE, #SC_CP_UTF8)
    ScintillaSendMessage(gadget_id, #SCI_STYLESETFONT, #STYLE_DEFAULT, MakeScintillaText("Lucida Console"))
    ScintillaSendMessage(gadget_id, #SCI_STYLESETSIZE, #STYLE_DEFAULT, 10)
    ScintillaSendMessage(gadget_id, #SCI_SETWRAPMODE, #SC_WRAP_WORD)
    ScintillaSendMessage(gadget_id, #SCI_SETWRAPVISUALFLAGS, #SC_WRAPVISUALFLAG_END)
    ScintillaSendMessage(gadget_id, #SCI_SETLAYOUTCACHE, 1)
    ; ScintillaSendMessage(gadget_id,#SCI_STYLESETBACK, #STYLE_DEFAULT,  RGB(250, 250, 250)) ; Цвет фона

    ; Подсветка синтаксиса
    ScintillaSendMessage(gadget_id, #SCI_STYLESETFORE, Styling::#LexerState_Keyword, 0)  ; Цвет обычного текста
        
    ScintillaSendMessage(gadget_id, #SCI_STYLESETFORE, Styling::#Lexer_Kludge, RGB(128, 0, 0))
    ScintillaSendMessage(gadget_id, #SCI_STYLESETBOLD, Styling::#Lexer_Kludge, 1)
    
    ScintillaSendMessage(gadget_id, #SCI_STYLESETFORE, Styling::#Lexer_Quote1, RGB(0, 128, 0))
    ScintillaSendMessage(gadget_id, #SCI_STYLESETBOLD, Styling::#Lexer_Quote1, 1)
    
    ScintillaSendMessage(gadget_id, #SCI_STYLESETFORE, Styling::#Lexer_Quote2, RGB(0, 0, 128))
    ScintillaSendMessage(gadget_id, #SCI_STYLESETBOLD, Styling::#Lexer_Quote2, 1)
    
    ScintillaSendMessage(gadget_id, #SCI_STYLESETFORE, Styling::#Lexer_Signature, $009966)
    ScintillaSendMessage(gadget_id, #SCI_STYLESETBOLD, Styling::#Lexer_Signature, 1)
    
  EndProcedure
  
  ; Величина отступа гаджетов от границ контейнеров.
  Global GW_Indent = 5
  
  ; Величина отступа просмоторщика сообщения от информации о сообщении.
  Global GW_MsgIndent = 90
  
  ; Размер тулбара.
  Global GW_Toolbar
  
  ; Размер меню-бар.
  Global GW_MenuHeight = MenuHeight()
  
  Procedure ToolBarText(tb_id,ButtonID,Text.s) 
    CompilerSelect #PB_Compiler_OS
      CompilerCase #PB_OS_Windows
        hwnd = ToolBarID(tb_id)
        tbi.TBBUTTONINFO 
        tbi\cbSize = SizeOf(TBBUTTONINFO) 
        tbi\dwMask = #TBIF_TEXT | #TBIF_STYLE 
        tbi\fsStyle = #BTNS_BUTTON | #BTNS_AUTOSIZE 
        tbi\pszText = @Text 
        SendMessage_(hwnd,#TB_SETBUTTONINFO,ButtonID,tbi)
        SendMessage_(hwnd,#TB_SETBUTTONSIZE,0, 22 | 22 << 16) 
        SendMessage_(hwnd,#TB_AUTOSIZE,0,0)
      CompilerCase #PB_OS_Linux
        Debug "Пришло время разбираться в GTK!11"
    CompilerEndSelect
  EndProcedure 
  
  Procedure ResizeAllGadgets()
    
    ResizeGadget( #GW_Splitter_AreasAndMsgs, #PB_Ignore, #PB_Ignore, 
                  WindowWidth(#Window_General), WindowHeight(#Window_General) )
    
    ResizeGadget( #GW_ListIcon_Areas, 
                  #PB_Ignore, #PB_Ignore, 
                  GadgetWidth(#GW_Container_Areas)-GW_Indent, GadgetHeight(#GW_Container_Areas)-(GW_Toolbar+GW_Indent+GW_MenuHeight) )
    
    TreeView::resizeTree( *GW_MessagesList, 0, 0, 
                          GadgetWidth(#GW_Container_Messages), GadgetHeight(#GW_Container_Messages) )
    
    If IsGadget(#GW_Splitter_ListAndWiew_H)
      ResizeGadget( #GW_Splitter_ListAndWiew_H, #PB_Ignore, #PB_Ignore, 
                    GadgetWidth(#GW_Container_ListAndWiew)-GW_Indent, GadgetHeight(#GW_Container_ListAndWiew)-(GW_Toolbar+GW_Indent+GW_MenuHeight) )
    EndIf
    
    If IsGadget(#GW_Splitter_ListAndWiew_V)
      ResizeGadget( #GW_Splitter_ListAndWiew_V, #PB_Ignore, #PB_Ignore, 
                    GadgetWidth(#GW_Container_ListAndWiew)-GW_Indent, GadgetHeight(#GW_Container_ListAndWiew)-(GW_Toolbar+GW_Indent+GW_MenuHeight) )
    EndIf  
      
    ResizeGadget( #GW_Scintilla_WiewMessage, #PB_Ignore, #PB_Ignore, 
                  GadgetWidth(#GW_Container_WiewMsg), GadgetHeight(#GW_Container_WiewMsg)-GW_MsgIndent )

  EndProcedure
  
  Procedure GW_Toolbar()
    LoadImage(#Img_PreviousMsg, "icons/go-previous.png")
    LoadImage(#Img_NextMsg, "icons/go-next.png")
    LoadImage(#Img_RefrashAreas, "icons/view-refresh.png")
    
    LoadImage(#Img_NewMsg, "icons/mail-message-new.png")
    LoadImage(#Img_ReplyMsg, "icons/mail-reply-sender.png")
    LoadImage(#Img_SendMail, "icons/mail-send-receive.png")
    
    CreateToolBar(#GW_Toolbar, WindowID(#Window_General))
    CompilerIf #PB_Compiler_OS = #PB_OS_Windows
      SendMessage_(ToolBarID(#GW_Toolbar),#TB_SETINDENT,4,0)
      SetWindowLongPtr_(ToolBarID(#GW_Toolbar), #GWL_STYLE, GetWindowLongPtr_(ToolBarID(#GW_Toolbar), #GWL_STYLE) | #TBSTYLE_LIST) 
      TBID = SendMessage_(ToolBarID(#GW_Toolbar),#TB_GETIMAGELIST,0,0) 
      ImageList_SetIconSize_(TBID, 16, 16) 
      SendMessage_(ToolBarID(#GW_Toolbar),#TB_SETIMAGELIST,0,TBID) 
      SendMessage_(ToolBarID(#GW_Toolbar),#TB_AUTOSIZE,0,0)
    CompilerEndIf
    
    ToolBarImageButton(#GW_Fun_PrevMsg, ImageID(#Img_PreviousMsg))
    ToolBarImageButton(#GW_Fun_NextMsg, ImageID(#Img_NextMsg))
    ToolBarImageButton(#GW_Fun_RefrashAreas, ImageID(#Img_RefrashAreas))
    
    ToolBarSeparator()
    
    ToolBarImageButton(#GW_Fun_CreateMsg, ImageID(#Img_NewMsg))
    ToolBarText(#GW_Toolbar, #GW_Fun_CreateMsg, "Новое сообщение") 
    
    ToolBarImageButton(#GW_Fun_ReplyMsg, ImageID(#Img_ReplyMsg))
    ToolBarText(#GW_Toolbar, #GW_Fun_ReplyMsg, "Ответ") 
    
    ToolBarSeparator()
    
    ToolBarImageButton(#GW_Fun_Run_SendMail, ImageID(#Img_SendMail))
    ToolBarText(#GW_Toolbar, #GW_Fun_Run_SendMail, "Проверить почту") 
    
    GW_Toolbar = ToolBarHeight(#GW_Toolbar)
  EndProcedure
  
  Procedure GW_Container_Areas()
    ListIconGadget(#GW_ListIcon_Areas, GW_Indent, 0, 245, 447, "#", 35, #PB_ListIcon_FullRowSelect | #PB_ListIcon_AlwaysShowSelection)
    AddGadgetColumn(#GW_ListIcon_Areas, 1, "Область", 115)
    AddGadgetColumn(#GW_ListIcon_Areas, 2, "Новых", 45)
    AddGadgetColumn(#GW_ListIcon_Areas, 3, "Всего", 45)
  EndProcedure
  
  Procedure GW_Container_MsgsList()
    *GW_MessagesList = TreeView::create(0, 0, 541, 150)
    *GW_MessagesList\header(0)\text = "Тема"
    *GW_MessagesList\header(1)\text = "От -> Кому"
    *GW_MessagesList\header(2)\text = "Дата"
    *GW_MessagesList\header(3)\text = "#"
    
    Define Config.TreeView::ct_customizing
    With Config
      \fontId = LoadFont(#PB_Any, "Arial", 9)
      \foregroundColor = RGB(60,60,60)
      \backgroundColor = RGB(250,250,250)
      \selForegroundColor = RGB(255,255,255)
      \selBackgroundColor = RGB(160,160,160)
      \lineColor = #PB_Ignore
      \headerBackroundColor = #PB_Ignore
      \headerForegroundColor = #PB_Ignore
    EndWith
    TreeView::customize(*GW_MessagesList, Config)
    ; Добавление строчки необходимо чтоб отобразились столбцы и они корректно изменилисвой размер.
    TreeView::addItem(*GW_MessagesList, TreeView::#parent_root, ""+#TAB$+""+#TAB$+""+#TAB$+"")
    TreeView::clearAllItems(*GW_MessagesList)
    TreeView::resizeHeader(GUI::*GW_MessagesList, 0, 300)
    TreeView::resizeHeader(GUI::*GW_MessagesList, 1, 210)
    TreeView::resizeHeader(GUI::*GW_MessagesList, 2, 125)
    TreeView::resizeHeader(GUI::*GW_MessagesList, 3, 60)
    TreeView::redraw(GUI::*GW_MessagesList)
  EndProcedure
  
  Procedure GW_Container_WiewMsg()
    TextGadget(#GW_MsgInfo_N_Msg, 5, 5, 50, 20, "Сообщ. :", #PB_Text_Right)
    TextGadget(#GW_MsgInfo_N_From, 5, 25, 50, 20, "От :", #PB_Text_Right)
    TextGadget(#GW_MsgInfo_N_To, 5, 45, 50, 20, "Кому :", #PB_Text_Right)
    TextGadget(#GW_MsgInfo_N_Subj, 5, 65, 50, 20, "Тема :", #PB_Text_Right)
    
    TextGadget(#GW_MsgInfo_Atr, 385, 5, 150, 20, "")
    TextGadget(#GW_MsgInfo_Subj, 65, 65, 625, 20, "")
    TextGadget(#GW_MsgInfo_Msg, 65, 5, 315, 20, "")
    TextGadget(#GW_MsgInfo_From, 65, 25, 150, 20, "")
    TextGadget(#GW_MsgInfo_To, 65, 45, 150, 20, "")
    TextGadget(#GW_MsgInfo_To_FTN, 220, 45, 100, 20, "")
    TextGadget(#GW_MsgInfo_From_FTN, 220, 25, 100, 20, "")
    
    TextGadget(#GW_MsgInfo_DateReceipt, 325, 25, 150, 20, "", #PB_Text_Right)
    ScintillaGadget(#GW_Scintilla_WiewMessage, 0, GW_MsgIndent, 541, 203, @SCI_Callback())
    SetScintillaStyle(#GW_Scintilla_WiewMessage)
  EndProcedure
  
  Procedure GW_Menu()
    If CreateMenu(#GW_MenuBar, WindowID(#Window_General)) 
      MenuTitle("Вид")
        MenuItem(#GW_Fun_UI_ShowOnlyUnread, "Отображать только непрочитанное")
        SetMenuItemState(#GW_MenuBar, #GW_Fun_UI_ShowOnlyUnread, Window\show_only_unread)
        OpenSubMenu("Вид списка сообщений")  
          MenuItem(#GW_Fun_UI_MsgsWiew_List, "Список сообщений"+Chr(9)+"F9")
            AddKeyboardShortcut(#Window_General, #PB_Shortcut_F9, #GW_Fun_UI_MsgsWiew_List)
            SetMenuItemState(#GW_MenuBar, #GW_Fun_UI_MsgsWiew_List, 1)
          MenuItem(#GW_Fun_UI_MsgsWiew_Tree, "Дерево сообщений"+Chr(9)+"F8")
            AddKeyboardShortcut(#Window_General, #PB_Shortcut_F8, #GW_Fun_UI_MsgsWiew_Tree)
        CloseSubMenu()
        OpenSubMenu("Разбивка окна")
          MenuItem(#GW_Fun_UI_Classic, "Классический вид")
            SetMenuItemState(#GW_MenuBar, #GW_Fun_UI_Classic, 1)
          MenuItem(#GW_Fun_UI_Vertical, "Вертикальный вид")
        CloseSubMenu()
      MenuTitle("Навигация")
        MenuItem(#GW_Fun_PrevMsg, "Предыдущее сообщение")
          AddKeyboardShortcut(#Window_General, #PB_Shortcut_Left, #GW_Fun_PrevMsg)
        MenuItem(#GW_Fun_NextMsg, "Следующее сообщение")
          AddKeyboardShortcut(#Window_General, #PB_Shortcut_Right, #GW_Fun_NextMsg)
        MenuTitle("Область")
        MenuItem(#GW_Fun_MarkReadCurrentArea, "Отметить все сообщения как прочитанные") 
        MenuBar()
        MenuItem(#GW_Fun_RefrashAreas, "Пересканировать области"+Chr(9)+"F5")
          AddKeyboardShortcut(#Window_General, #PB_Shortcut_F5, #GW_Fun_RefrashAreas)    
      MenuTitle("Сообщение")
        MenuItem(#GW_Fun_CreateMsg, "Новое сообщение") 
        MenuItem(#GW_Fun_ReplyMsg, "Ответ") 
        MenuItem(#GW_Fun_SaveMsgInFile, "Сохранить сообщение в файл")
        MenuBar()
        MenuItem(#GW_Fun_Msg_Unread, "Отметить как непрочитанное")
        MenuItem(#GW_Fun_Msg_Delete, "Удалить сообщение")
      MenuTitle("Сервис")
        MenuItem(#GW_Fun_Run_SendMail, "Проверить почту")   
    EndIf
  EndProcedure

  ;- Можно, кстати, научить процедуру принимать переменную, заставляющая её скрывать окно.
  Procedure OpenWindow_General()
    UsePNGImageDecoder()
    OpenWindow(#Window_General, Window\x, Window\y, Window\w, Window\h, "Dozens", #PB_Window_SystemMenu | #PB_Window_SizeGadget | #PB_Window_Invisible | #PB_Window_TitleBar | #PB_Window_MinimizeGadget | #PB_Window_MaximizeGadget)
    ;SmartWindowRefresh(#Window_General, 1)
    SetWindowState(#Window_General, Window\state)
    GW_Menu()
    ;- GW_Toolbar()
    ContainerGadget(#GW_Container_Areas, 0, 0, 300, 400)
      GW_Container_Areas()
    CloseGadgetList()
    ContainerGadget(#GW_Container_ListAndWiew, 0, 0, 546, 500)
      ContainerGadget(#GW_Container_Messages, 0, 0, 541, 150)
        GW_Container_MsgsList()
      CloseGadgetList()
      ContainerGadget(#GW_Container_WiewMsg, 0, 0, 541, 243)
        GW_Container_WiewMsg()
      CloseGadgetList()
      SplitterGadget(#GW_Splitter_ListAndWiew_H, 0, 0, 541, 447, #GW_Container_Messages, #GW_Container_WiewMsg)
    CloseGadgetList()
    SplitterGadget(#GW_Splitter_AreasAndMsgs, 0, GW_Toolbar, 800, 500, #GW_Container_Areas, #GW_Container_ListAndWiew, #PB_Splitter_Vertical)

    CompilerIf #PB_Compiler_OS = #PB_OS_Windows
      SetWindowLongPtr_(GadgetID(#GW_Container_Areas), #GWL_STYLE, 
                        GetWindowLongPtr_(GadgetID(#GW_Container_Areas), #GWL_STYLE) | #WS_CLIPCHILDREN)
      SetWindowLongPtr_(GadgetID(#GW_Container_ListAndWiew), #GWL_STYLE, 
                        GetWindowLongPtr_(GadgetID(#GW_Container_ListAndWiew), #GWL_STYLE) | #WS_CLIPCHILDREN)
      SetWindowLongPtr_(GadgetID(#GW_Container_Messages), #GWL_STYLE, 
                      GetWindowLongPtr_(GadgetID(#GW_Container_Areas), #GWL_STYLE) | #WS_CLIPCHILDREN)
      SetWindowLongPtr_(GadgetID(#GW_Container_WiewMsg), #GWL_STYLE, 
                        GetWindowLongPtr_(GadgetID(#GW_Container_WiewMsg), #GWL_STYLE) | #WS_CLIPCHILDREN)
      SetWindowLongPtr_(GadgetID(#GW_Splitter_AreasAndMsgs), #GWL_STYLE, 
                        GetWindowLongPtr_(GadgetID(#GW_Container_Areas), #GWL_STYLE) | #WS_CLIPCHILDREN)
    CompilerEndIf

    BindEvent(#PB_Event_SizeWindow, @ResizeAllGadgets()) 
    BindEvent(#PB_Event_Gadget, @ResizeAllGadgets(), #Window_General, #GW_Splitter_AreasAndMsgs)
    BindEvent(#PB_Event_Gadget, @ResizeAllGadgets(), #Window_General, #GW_Splitter_ListAndWiew_H)
    BindEvent(#PB_Event_Gadget, @ResizeAllGadgets(), #Window_General, #GW_Splitter_ListAndWiew_V)

    If Window\type = #Vertical
      SwitchTypeUI(#Vertical)
    EndIf  
    
    If Window\msgs_mode = #Tree
      SwitchMsgsViewType(#Tree)
    EndIf  

    SetGadgetState(#GW_Splitter_AreasAndMsgs, Window\splitter1)
    ResizeAllGadgets()
    
    If GUI::Window\type = GUI::#Classic
      SetGadgetState(#GW_Splitter_ListAndWiew_H, Window\splitter2)
    Else
      SetGadgetState(#GW_Splitter_ListAndWiew_V, Window\splitter2)
    EndIf

    ResizeAllGadgets()
    HideWindow(GUI::#Window_General, 0)
  EndProcedure
  
  ; Собственно, так мы отображаем текст сообщения.
  Procedure SetTextInWiewMessage(text.s)
    ScintillaSendMessage(#GW_Scintilla_WiewMessage, #SCI_SETREADONLY, 0)
    ScintillaSendMessage(#GW_Scintilla_WiewMessage, #SCI_SETTEXT, 0, MakeScintillaText(text.s))
    ScintillaSendMessage(#GW_Scintilla_WiewMessage, #SCI_SETREADONLY, 1)
  EndProcedure
  
    ; Вернёт текст, находящийся в редакторе сообщений.
  Procedure.s GetTextFromEditorMsg()
    Protected sciText.s
    length = ScintillaSendMessage(#EW_Editor_Msg, #SCI_GETLENGTH)
    sciText.s = Space(length + 1)
    ScintillaSendMessage(#EW_Editor_Msg, #SCI_GETTEXT, length + 1, @sciText)
    ProcedureReturn PeekS(@sciText, length + 1, #PB_UTF8)
  EndProcedure
  
  ; А так текст добавляется в редактор сообщений.
  Procedure SetTextInEditorMessage(text.s)
    ScintillaSendMessage(#GW_Scintilla_WiewMessage, #SCI_SETREADONLY, 0)
    ScintillaSendMessage(#EW_Editor_Msg, #SCI_SETTEXT, 0, MakeScintillaText(text.s))
    ScintillaSendMessage(#GW_Scintilla_WiewMessage, #SCI_SETREADONLY, 1)
  EndProcedure
  

  
  ; Переключает вид гаджетов в #GW_Splitter_ListAndWiew между гориз. и верт. отображением.
  ; type может принимать значения #Vertical или #Classic.
  Procedure SwitchTypeUI(type)
    OpenGadgetList(#GW_Container_ListAndWiew)
    If type = #Vertical ;And Window\type <> #Vertical
      Window\type = #Vertical
      SplitterGadget(#GW_Splitter_ListAndWiew_V, 0, 0, 541, 447, #GW_Container_Messages, #GW_Container_WiewMsg, #PB_Splitter_Vertical)
      FreeGadget(#GW_Splitter_ListAndWiew_H)
      SetMenuItemState(#GW_MenuBar, #GW_Fun_UI_Classic, 0)
      SetMenuItemState(#GW_MenuBar, #GW_Fun_UI_Vertical, 1)
    ElseIf type = #Classic ;And Window\type <> #Classic
      Window\type = #Classic
      SplitterGadget(#GW_Splitter_ListAndWiew_H, 0, 0, 541, 447, #GW_Container_Messages, #GW_Container_WiewMsg)
      FreeGadget(#GW_Splitter_ListAndWiew_V)
      SetMenuItemState(#GW_MenuBar, #GW_Fun_UI_Classic, 1)
      SetMenuItemState(#GW_MenuBar, #GW_Fun_UI_Vertical, 0)
    EndIf
    CloseGadgetList()
    ResizeAllGadgets()
  EndProcedure
  
  ; Переключает вид панели сообщений.
  ; type может принимать значения #List или #Tree.
  Procedure SwitchMsgsViewType(type)
    If type = #List ;And Window\msgs_mode <> #List
      Window\msgs_mode = #List
      ;- Здесь нужно будет запилить очистку дерева и построение списка мессаг.
      
      SetMenuItemState(#GW_MenuBar, #GW_Fun_UI_MsgsWiew_List, 1)
      SetMenuItemState(#GW_MenuBar, #GW_Fun_UI_MsgsWiew_Tree, 0)
    ElseIf type = #Tree ;And Window\msgs_mode <> #Tree
      Window\msgs_mode = #Tree
      ;- Здесь нужно будет запилить очистку списка мессаг и построение дерева.
      
      SetMenuItemState(#GW_MenuBar, #GW_Fun_UI_MsgsWiew_List, 0)
      SetMenuItemState(#GW_MenuBar, #GW_Fun_UI_MsgsWiew_Tree, 1)
    EndIf
    ResizeAllGadgets()
  EndProcedure
  
  ; Заблокирует пункты меню, позволяющие создавать новые сообщения. 
  ; Используется при открытии OpenWindow_Editor().
  Procedure LockCreateMsgFunctions()
    DisableMenuItem(GUI::#GW_MenuBar, GUI::#GW_Fun_CreateMsg, 1)
    DisableMenuItem(GUI::#GW_MenuBar, GUI::#GW_Fun_ReplyMsg, 1)
    ;DisableToolBarButton(GUI::#GW_Toolbar, GUI::#GW_Fun_CreateMsg, 1)
    ;DisableToolBarButton(GUI::#GW_Toolbar, GUI::#GW_Fun_ReplyMsg, 1)
  EndProcedure
  
  ; Разблокирует пункты меню, позволяющие создавать новые сообщения. 
  ; Используется при закрытии OpenWindow_Editor().
  Procedure UnlockCreateMsgFunctions()
    DisableMenuItem(GUI::#GW_MenuBar, GUI::#GW_Fun_CreateMsg, 0)
    DisableMenuItem(GUI::#GW_MenuBar, GUI::#GW_Fun_ReplyMsg, 0)
    ;DisableToolBarButton(GUI::#GW_Toolbar, GUI::#GW_Fun_CreateMsg, 0)
    ;DisableToolBarButton(GUI::#GW_Toolbar, GUI::#GW_Fun_ReplyMsg, 0)
  EndProcedure
  
  Procedure OpenWindow_Editor()
    OpenWindow(#Window_Editor, 105, 54, 586, 416, "Создание сообщения", #PB_Window_ScreenCentered)
    StringGadget(#EW_String_From, 90, 10, 244, 18, "")
    StringGadget(#EW_String_From_Ftn, 340, 10, 224, 18, "")
    StringGadget(#EW_String_To, 90, 30, 244, 18, "")
    StringGadget(#EW_String_To_Ftn, 340, 30, 224, 18, "")
    StringGadget(#EW_String_Subj, 90, 50, 474, 18, "")
    
    ;- [Scintilla] EditorGadget(#EW_Editor_Msg, 20, 80, 542, 264, #PB_Editor_WordWrap)
    ScintillaGadget(#EW_Editor_Msg, 20, 80, 542, 264, @SCI_Callback())
    SetScintillaStyle(#EW_Editor_Msg)
    
    ButtonGadget(#EW_Button_OK, 420, 350, 142, 24, "Сохранить")
    ButtonGadget(#EW_Button_Cancel, 420, 380, 142, 24, "Отмена")
    
    TextGadget(#PB_Any, 20, 12, 62, 14, "От:")
    TextGadget(#PB_Any, 20, 32, 62, 14, "Кому:")
    TextGadget(#PB_Any, 20, 52, 62, 14, "Тема:")
  EndProcedure
  
EndModule
; IDE Options = PureBasic 5.31 (Windows - x86)
; CursorPosition = 477
; FirstLine = 276
; Folding = 8B6P-
; EnableUnicode
; EnableXP