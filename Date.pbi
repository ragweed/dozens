﻿
DeclareModule Date
  
  Declare UTCTime()
  Declare.s GetTimeBias()
  Declare.s CreateDateString(date.q)
  
EndDeclareModule

Module Date

  ; Для UTCTime().
  CompilerIf Defined(timezone, #PB_Structure) = #False
    Structure timezone
      tz_minuteswest.l
      tz_dsttime.l
    EndStructure
  CompilerEndIf
  
  ; Вернёт текущее время в UTC.
  Procedure UTCTime()
    ; http://www.purebasic.fr/german/viewtopic.php?f=3&t=24390
    CompilerSelect #PB_Compiler_OS
      CompilerCase #PB_OS_Windows
        Protected UtcOs.TIME_ZONE_INFORMATION
        GetTimeZoneInformation_(@UtcOs)
        ProcedureReturn Date() + (UtcOs\Bias)*60
      CompilerCase #PB_OS_Linux
        Protected tv.q, tz.timezone
        gettimeofday_(@tv, @tz)
        ProcedureReturn Date() + tz\tz_minuteswest*60
      CompilerCase #PB_OS_MacOS
        ProcedureReturn Date()
    CompilerEndSelect 
  EndProcedure
  
  ; Вернёт смешение системного времени относительно UTC в строке в формате [-]ЧЧMM.
  Procedure.s GetTimeBias()
    system_date = Date()
    utc_date = UTCTime()
    If system_date>utc_date
      time_offset = system_date-utc_date
      result.s = FormatDate("%hh%ii", time_offset)
    ElseIf system_date<utc_date
      time_offset = utc_date-system_date
      result.s = "-"+FormatDate("%hh%ii", time_offset)
    ElseIf system_date=utc_date  
      ;- А можно ли проигнорить TZUTC, если смещение равно 0?
      result.s = "0000"
    EndIf
    ProcedureReturn result.s
  EndProcedure
  
  ; Возращает дату в виде строки.
  Procedure.s  CreateDateString(date.q)
    date_int = date.q/1000
    Time.s = FormatDate("%hh:%ii:%ss", date_int)
    Year.s = FormatDate("%yy", date_int)
    Month.s = FormatDate("%mm", date_int)
    Select Month.s
      Case "01" : Month.s = "Jan"
      Case "02" : Month.s = "Feb"
      Case "03" : Month.s = "Mar"
      Case "04" : Month.s = "Apr"
      Case "05" : Month.s = "May"
      Case "06" : Month.s = "Jun"
      Case "07" : Month.s = "Jul"
      Case "08" : Month.s = "Aug"
      Case "09" : Month.s = "Sep"
      Case "10" : Month.s = "Oct"
      Case "11" : Month.s = "Nov"
      Case "12" : Month.s = "Dec"
    EndSelect
    Day.s = FormatDate("%dd", date_int)
    Output.s = Day.s+" "+Month.s+" "+Year.s+"  "+Time.s
    ProcedureReturn Output.s
  EndProcedure
  
EndModule
; IDE Options = PureBasic 5.30 (Windows - x86)
; CursorPosition = 78
; Folding = --
; EnableUnicode
; EnableXP