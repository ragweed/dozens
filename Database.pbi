﻿
DeclareModule DB
  
  Enumeration AreaType
    #All
    #Local
    #Netmail
    #Echomail
  EndEnumeration
  
  Structure Area
    fake_id.l
    area_type.l
    echoarea_id.l
    name.s
    
    lastread.l
    all.l
    unread.l
  EndStructure
  
  Structure Message
    fake_id.l
    index_id.l
    id.l
    area_type.l
    echoarea_id.l
    from_name.s
    to_name.s
    from_ftn_addr.s
    to_ftn_addr.s
    date.q
    subject.s
    message.s
    
    msgid.s
    reply.s
    
    read_msg.a
    
    tw_entry.i
  EndStructure
  
  Structure Tree
    id.l
    fake_id.l
    date.f
    List Reply.Tree()
  EndStructure
  
  NewList AreasList.Area()
  NewList MsgsList.Message()
  msg.Message
  Tree.Tree
  
  Declare Init(database_path.s)
  Declare GetAreasList(List AreasList.Area(), list_type=#All)
  Declare GetEchomailList(List MsgsList.Message(), echoarea_id, only_unread=0)
  Declare GetNetMailList(List MsgsList.Message(), only_unread=0)
  Declare GetEchomailMsg(*msg.Message, echoarea_id, msg_id)
  Declare GetNetMailMsg(*msg.Message, msg_id)
  Declare CreateNetmailMsg(*msg.Message)
  Declare CreateMsgsTree(List MsgsList.Message(), *Tree.Tree)
  Declare CreateEchomailMsg(*msg.Message)
  Declare DeleteMessage(msg_type, msg_id, echoarea_id=0)
  Declare MarkRead(area_type, msg_id, echoarea_id=0)
  Declare MarkUnread(area_type, msg_id, echoarea_id=0)
  Declare GetLastreadMsg(msg_type, echoarea_id=0)
  Declare SetLastreadMsg(msg_id, msg_type, echoarea_id=0)
  Declare MarkAllRead(area_type, echoarea_id=0)
  
EndDeclareModule

Module DB
  
  UseSQLiteDatabase()
  
  ; Здесь будем хранить хэндл БД.
  Global DatabaseID
  
  ; А вот здесь хранится последние сообщение о ошибке БД.
  Global DatabaseError.s
  
  ; Создаёт в БД дополнительные таблицы \ добавляет столбцы в уже сущ. таблицы в которых будет храниться вспомогательная информация
  ; необходимая именно для Dozens. Например, прочитанно ли сообщение, последнее прочитанное сообщение в арии и т.д.
  Procedure FixDBForDozens()
    requests.s = "BEGIN TRANSACTION;"
    requests.s + "CREATE TABLE netmail_lastread (last_read INTIGER NOT NULL DEFAULT 0); "
    requests.s + "ALTER TABLE echoarea ADD COLUMN last_read INTIGER NOT NULL DEFAULT 0; "
    requests.s + "ALTER TABLE echomail ADD COLUMN read INTIGER NOT NULL DEFAULT 0; "
    requests.s + "ALTER TABLE netmail ADD COLUMN read INTIGER NOT NULL DEFAULT 0; "
    requests.s + "INSERT INTO netmail_lastread VALUES (0); "
    requests.s + "CREATE TABLE dozens (info); "
    requests.s + "INSERT INTO dozens VALUES (1); "
    requests.s + "COMMIT;"

    If DatabaseUpdate(DatabaseID, requests.s)
      FinishDatabaseQuery(DatabaseID)
      result = 1
    Else
      DatabaseError.s = DatabaseError()
      Debug "DB::FixDBForDozens() > error > "+DatabaseError()
      result = 0
    EndIf
    
    ProcedureReturn result
  EndProcedure
  
  ; Вспомогательная процедура для Init().
  Procedure OpenDB(db_name.s)
    DatabaseID = OpenDatabase(#PB_Any, db_name.s, "", "", #PB_Database_SQLite)
    If DatabaseID<>0
      ; Проверяем, вносились ли уже изменения в БД функцией FixDBForDozens().
      If Not DatabaseQuery(DatabaseID, "SELECT * FROM dozens;")
        FixDBForDozens()
      EndIf
    EndIf
    ProcedureReturn DatabaseID
  EndProcedure
  
  ; Открывает Sqlite БД Jnode, делая доступным использование функций из данного модуля.
  Procedure Init(database_path.s)
    ; Проверяем, не была ли инициализирована БД ранее.
    If DatabaseID = 0
      If ReadFile(0, database_path.s)
        CloseFile(0)
        If Not OpenDB(database_path.s)
          Debug "DB::Init() > critical error > "+DatabaseError()
          MessageRequester("DB::Init()", "Невозможно открыть базу данных."+Chr(10)+DatabaseError()+Chr(10)+"Приложение будет закрыто.")
          End
        EndIf
      Else
        If OpenDB(database_path.s)
          Debug "DB::Init() > critical error > unknow error"
          MessageRequester("DB::Init()", "Невозможно открыть базу данных."+Chr(10)+"Вероятно, база данных занята другим приложением."+Chr(10)+"Приложение будет закрыто.")
          End
        Else
          Debug "DB::Init() > critical error > "+DatabaseError()
          MessageRequester("DB::Init()", "Базы данных не существует."+Chr(10)+"Приложение будет закрыто.")
          End
        EndIf
      EndIf
      ProcedureReturn 1
    Else
      ProcedureReturn 0
    EndIf
  EndProcedure
  
  ; Загружает из БД в AreasList() определённый (переменной list_type) список арий.
  ; list_type может принимать следующие значения:
  ; #All - все арии;
  ; #Local - только локальные;
  ; #Echomail - только эхи.
  Procedure GetAreasList(List AreasList.Area(), list_type=#All)
    ClearList(AreasList())
    
    Enumeration Columns
      #id
      #name
      #count
      #read
    EndEnumeration
    
    netmail.s = "SELECT (0) id, ('netmail') name, "
    netmail.s + "	(SELECT COUNT(*) FROM netmail) count, "
    netmail.s + "	(SELECT COUNT(*) FROM netmail WHERE read=1) read "
    
    echomail.s = "SELECT id, name, "
    echomail.s + "	(SELECT COUNT(*) FROM echomail WHERE echomail.echoarea_id=echoarea.id) count, "
    echomail.s + "	(SELECT COUNT(*) FROM echomail WHERE echomail.echoarea_id=echoarea.id AND read=1) read "
    echomail.s + "	FROM echoarea "
    echomail.s + "ORDER BY id ASC"
    
    If list_type = #All
      query.s = netmail.s+Chr(10)
      query.s + "UNION"+Chr(10)
      query.s + echomail.s+";"
    ElseIf list_type = #Local
      query.s = netmail.s+";"
    ElseIf list_type = #Echomail
      query.s = echomail.s+";"
    EndIf
    
    If query.s<>""
      If DatabaseQuery(DatabaseID, query.s)
        While NextDatabaseRow(DatabaseID)
          AddElement(AreasList())
          If GetDatabaseString(DatabaseID, #name) = "netmail"
            AreasList()\area_type   = #Netmail
            AreasList()\name        = "netmail"
            AreasList()\fake_id     = ListIndex(AreasList())
          Else
            AreasList()\area_type   = #Echomail
            AreasList()\echoarea_id = GetDatabaseLong(DatabaseID, #id)
            AreasList()\name        = GetDatabaseString(DatabaseID, #name)
            AreasList()\fake_id     = ListIndex(AreasList())
          EndIf
          AreasList()\all         = GetDatabaseLong(DatabaseID, #count)
          AreasList()\unread      = AreasList()\all - GetDatabaseLong(DatabaseID, #read)
        Wend
        FinishDatabaseQuery(DatabaseID)
        result = ListSize(AreasList())
      Else
        DatabaseError.s = DatabaseError()
        Debug "DB::GetAreasList() > error > "+DatabaseError.s
        result = 0
      EndIf
    EndIf
    
    ProcedureReturn result
  EndProcedure
  
  ; Загружает из БД в MsgsList() список писем из эхоконференции echoarea_id.
  ; Если only_unread=1, вернёт только непрочитанные (и последнее прочитанное) сообщения.
  Procedure GetEchomailList(List MsgsList.Message(), echoarea_id, only_unread=0)
    start_time = ElapsedMilliseconds()
    ClearList(MsgsList())

    columns.s = "id, "
    columns.s + "echoarea_id, "
    columns.s + "from_name, "
    columns.s + "to_name, "
    columns.s + "from_ftn_addr, "
    columns.s + "date, "
    columns.s + "subject, "
    columns.s + "msgid, "
    columns.s + "message, "
    columns.s + "read "
    
    If only_unread=1
      ; Вы можете, конечно, задаться вопросом: -"Найхуа такой йзъйобъ?77".
      ; Увы, вариант с "SELECT * FROM echomail WHERE echoarea_id=%echoarea_id% 
      ; AND read<>1 OR id=%last_read% ORDER BY date asc; не работает из-за NULL значений в столбце.
      ; И почему они там появляются - для меня загадка, так что пусть будет так! ^_^
      ;- Не забудь спросить про такое поведние и можно ли его юзать!
      query.s = "SELECT "+columns.s+" FROM echomail "
      query.s + "  WHERE echoarea_id="+echoarea_id+" "
      query.s + "EXCEPT "
      query.s + "SELECT "+columns.s+" FROM echomail "
      query.s + "  WHERE echoarea_id="+echoarea_id+" "
      query.s + "    AND read=1 AND id!=(SELECT last_read FROM echoarea WHERE id="+echoarea_id+") "
      query.s + "ORDER BY date asc;"
    Else
      query.s = "SELECT "+columns.s+" FROM echomail "
      query.s + "WHERE echoarea_id="+echoarea_id+" ORDER BY date asc;"
    EndIf  

    If DatabaseQuery(DatabaseID, query.s)
      While NextDatabaseRow(DatabaseID)
        AddElement(MsgsList())
        MsgsList()\fake_id        = ListSize(MsgsList())-1
        MsgsList()\id             = GetDatabaseLong(DatabaseID, 0)
        MsgsList()\echoarea_id    = GetDatabaseLong(DatabaseID, 1)
        MsgsList()\from_name      = GetDatabaseString(DatabaseID, 2)
        MsgsList()\to_name        = GetDatabaseString(DatabaseID, 3)
        MsgsList()\from_ftn_addr  = GetDatabaseString(DatabaseID, 4)
        MsgsList()\date           = GetDatabaseQuad(DatabaseID, 5)
        MsgsList()\subject        = GetDatabaseString(DatabaseID, 6)
        MsgsList()\msgid          = GetDatabaseString(DatabaseID, 7)
        MsgsList()\reply          = Kludges::SearchReplyKludge(GetDatabaseString(DatabaseID, 8))
        MsgsList()\read_msg       = GetDatabaseLong(DatabaseID, 9)
      Wend
      FinishDatabaseQuery(DatabaseID)
      result = ListSize(MsgsList())
      time.f = (ElapsedMilliseconds()-start_time)/1000
      Debug "Получил список из "+result+" сообщений за "+time
    Else
      DatabaseError.s = DatabaseError()
      Debug "DB::GetEchomailList() > error > "+DatabaseError()
      result = 0
    EndIf

    ProcedureReturn result
  EndProcedure

  ; Создаёт из MsgsList() дерево сообщений.
  Procedure CreateMsgsTree(List MsgsList.Message(), *Tree.Tree)
    start_time = ElapsedMilliseconds()
    ClearList(*Tree\Reply())

    ; В этой карте будут запоминатся ссылки на узлы дерева. В качестве ключа запоминается msgid.s.
    NewMap Msgid()
    
    ; Перебираем сообщения из MsgsList().
    ForEach MsgsList()
      ; Проверяем, является ли это сообщение ответом на какое-либо уже сохранённое в дереве сообщение,
      ; если да - мы получим ссылку на нужный узел в дереве.
      *tree_branch.Tree = Msgid(MsgsList()\reply)
      If *tree_branch = 0
        ; Данное сообщение оригинальное, добавляем его в корень дерева.
        AddElement(*Tree\Reply())
        *Tree\Reply()\id = MsgsList()\id
        *Tree\Reply()\fake_id = MsgsList()\fake_id
        *Tree\Reply()\date = MsgsList()\date
        ; Проверка msgid на корректность.
        If MsgsList()\msgid <> ""
          Msgid(MsgsList()\msgid) = *Tree\Reply()
        EndIf
      Else
        ; Ответвляем это сообщение от того, на которое оно является ответом.
        AddElement(*tree_branch\Reply())
        *tree_branch\Reply()\id = MsgsList()\id
        *tree_branch\Reply()\fake_id = MsgsList()\fake_id
        *Tree\Reply()\date = MsgsList()\date
        Msgid(MsgsList()\msgid) = *tree_branch\Reply()
      EndIf
    Next
    
    FreeMap(Msgid())
    
    time.f = (ElapsedMilliseconds()-start_time)/1000
    Debug "На создание дерева из "+ListSize(MsgsList())+" сообщений ушло "+time
  EndProcedure
  
  ; Загружает из БД в MsgsList() список писем из арии NetMail.
  ; Если only_unread=1, вернёт только непрочитанные (и последнее прочитанное) сообщения.
  Procedure GetNetMailList(List MsgsList.Message(), only_unread=0)
    ClearList(MsgsList())

    columns.s = "id, "
    columns.s + "from_name, "
    columns.s + "to_name, "
    columns.s + "from_address, "
    columns.s + "to_address, "
    columns.s + "date, "
    columns.s + "subject, "
    columns.s + "text, "
    columns.s + "read "
    
    If only_unread=1
      unread.s = "WHERE (read=0 OR id=(SELECT * FROM netmail_lastread))"
    EndIf 
    
    If only_unread=1
      ; Вы можете, конечно, задаться вопросом: -"Найхуа такой йзъйобъ?77".
      ; Увы, вариант с "SELECT * FROM echomail WHERE echoarea_id=%echoarea_id% 
      ; AND read<>1 OR id=%last_read% ORDER BY date asc; не работает из-за NULL значений в столбце.
      ; И почему они там появляются - для меня загадка, так что пусть будет так! ^_^
      ;- Не забудь спросить про такоеповедние и можно ли его юзать!
      query.s = "SELECT "+columns.s+" FROM netmail "
      query.s + "EXCEPT "
      query.s + "SELECT "+columns.s+" FROM netmail "
      query.s + "  WHERE read=1 AND id!=(SELECT * FROM netmail_lastread) "
      query.s + "ORDER BY date asc;"
    Else
      query.s="SELECT "+columns.s+" FROM netmail "+unread.s+" ORDER BY date asc;"
    EndIf 

    

    If DatabaseQuery(DatabaseID, query.s)
      While NextDatabaseRow(DatabaseID)
        AddElement(MsgsList())
        MsgsList()\fake_id        = ListSize(MsgsList())-1
        MsgsList()\id             = GetDatabaseLong(DatabaseID, 0)
        MsgsList()\from_name      = GetDatabaseString(DatabaseID, 1)
        MsgsList()\to_name        = GetDatabaseString(DatabaseID, 2)
        MsgsList()\from_ftn_addr  = GetDatabaseString(DatabaseID, 3)
        MsgsList()\to_ftn_addr    = GetDatabaseString(DatabaseID, 4)
        MsgsList()\date           = GetDatabaseQuad(DatabaseID, 5)
        MsgsList()\subject        = GetDatabaseString(DatabaseID, 6)
        
        MsgsList()\msgid          = Kludges::SearchMsgidKludge(GetDatabaseString(DatabaseID, 7))
        MsgsList()\reply          = Kludges::SearchReplyKludge(GetDatabaseString(DatabaseID, 7))
        
        MsgsList()\read_msg       = GetDatabaseLong(DatabaseID, 8)
      Wend
      FinishDatabaseQuery(DatabaseID)
      result = ListSize(MsgsList())
    Else
      DatabaseError.s = DatabaseError()
      Debug "DB::GetNetMailList() > error > "+DatabaseError()
      result = 0
    EndIf

    ProcedureReturn result
  EndProcedure
  
  ; Получает из БД письмо msg_id из эхоконференции echoarea_id.
  Procedure GetEchomailMsg(*msg.Message, echoarea_id, msg_id)
    columns.s = "id, "
    columns.s + "echoarea_id, "
    columns.s + "from_name, "
    columns.s + "to_name, "
    columns.s + "from_ftn_addr, "
    columns.s + "date, "
    columns.s + "subject, "
    columns.s + "message, "
    columns.s + "msgid, "
    columns.s + "read "
    
    query.s="SELECT "+columns.s+" FROM echomail WHERE echoarea_id="+echoarea_id+" And id="+msg_id+";"
    If DatabaseQuery(DatabaseID, query.s)
      If NextDatabaseRow(DatabaseID)
        *msg\id             = GetDatabaseLong(DatabaseID, 0)
        *msg\echoarea_id    = GetDatabaseLong(DatabaseID, 1)
        *msg\from_name      = GetDatabaseString(DatabaseID, 2)
        *msg\to_name        = GetDatabaseString(DatabaseID, 3)
        *msg\from_ftn_addr  = GetDatabaseString(DatabaseID, 4)
        *msg\date           = GetDatabaseQuad(DatabaseID, 5)
        *msg\subject        = GetDatabaseString(DatabaseID, 6)
        *msg\msgid          = GetDatabaseString(DatabaseID, 8)
        *msg\message        = Chr(1) + "MSGID: " + *msg\msgid + Chr(10) + GetDatabaseString(DatabaseID, 7)
        *msg\read_msg       = GetDatabaseLong(DatabaseID, 9)
        FinishDatabaseQuery(DatabaseID)
        result = 1
      EndIf
    Else
      DatabaseError.s = DatabaseError()
      Debug "DB::GetEchomailMsg() > error > "+DatabaseError()
      result = 0
    EndIf
    ProcedureReturn result
  EndProcedure  
  
  ; Получает из БД письмо msg_id из NetMail.
  Procedure GetNetMailMsg(*msg.Message, msg_id)
    columns.s = "id, "
    columns.s + "from_name, "
    columns.s + "to_name, "
    columns.s + "from_address, "
    columns.s + "to_address, "
    columns.s + "date, "
    columns.s + "subject, "
    columns.s + "text, "
    columns.s + "read "
    
    query.s="SELECT "+columns.s+" FROM netmail WHERE id="+msg_id+";"
    If DatabaseQuery(DatabaseID, query.s)
      If NextDatabaseRow(DatabaseID)
        *msg\id             = GetDatabaseLong(DatabaseID, 0)
        *msg\from_name      = GetDatabaseString(DatabaseID, 1)
        *msg\to_name        = GetDatabaseString(DatabaseID, 2)
        *msg\from_ftn_addr  = GetDatabaseString(DatabaseID, 3)
        *msg\to_ftn_addr    = GetDatabaseString(DatabaseID, 4)
        *msg\date           = GetDatabaseQuad(DatabaseID, 5)
        *msg\subject        = GetDatabaseString(DatabaseID, 6)
        *msg\message        = GetDatabaseString(DatabaseID, 7)
        *msg\read_msg       = GetDatabaseLong(DatabaseID, 8)
        FinishDatabaseQuery(DatabaseID)
        result = 1
      EndIf
    Else
      DatabaseError.s = DatabaseError()
      Debug "DB::GetNetMailMsg() > error > "+DatabaseError()
      result = 0
    EndIf
    ProcedureReturn result
  EndProcedure
  
  ; Возвращает msg_id последнего прочитанного в арии сообщения.
  ; msg_type может принимать следующие значения: #Netmail или #Echomail.
  ; Вернёт -1 в случае неудачи.
  Procedure GetLastreadMsg(msg_type, echoarea_id=0)
    Protected query.s, result
    
    If msg_type=#Netmail
      query.s="SELECT * FROM netmail_lastread;"
    ElseIf msg_type=#Echomail
      query.s="SELECT last_read FROM echoarea WHERE id="+echoarea_id+";"
    EndIf

    If DatabaseQuery(DatabaseID, query.s)
      If NextDatabaseRow(DatabaseID)
        result = GetDatabaseLong(DatabaseID, 0)
        FinishDatabaseQuery(DatabaseID)
      EndIf
    Else
      DatabaseError.s = DatabaseError()
      Debug "DB::GetLastreadMsg() > error > "+DatabaseError()
      result = -1
    EndIf
    ProcedureReturn result
  EndProcedure
  
  ; Сохраняет msg_id последнего прочитанного сообщения.
  ; Для нетмейла - msg_type = #Netmail, для эхи - msg_type = #Echomail и id эхи в echoarea_id.
  Procedure SetLastreadMsg(msg_id, msg_type, echoarea_id=0)
    If msg_type = #Netmail
      query.s="UPDATE netmail_lastread SET last_read="+msg_id+";"
    ElseIf msg_type = #Echomail
      query.s="UPDATE echoarea SET last_read="+msg_id+" WHERE id="+echoarea_id+";"
    EndIf

    If query.s<>""
      If DatabaseUpdate(DatabaseID, query.s)
        result = 1
      Else
        DatabaseError.s = DatabaseError()
        Debug "DB::SetLastreadMsg() > error > "+DatabaseError.s
        result = 0
      EndIf
      ProcedureReturn result
    EndIf
  EndProcedure  
    
  ; Меняет в строке "'" на "''".
  Procedure.s FixInvertedCommas(string.s)
    If FindString(string.s, "'")
      string.s = ReplaceString(string.s, "'", "''")
    EndIf
    ProcedureReturn string.s
  EndProcedure  
    
  ; Создаёт сообщение в арии netmail.
  ; Для создания сообщения необходимо заполнить следующие элементы структуры:
  ; *msg\from_name
  ;     \to_name
  ;     \from_ftn_addr
  ;     \to_ftn_addr
  ;     \date
  ;     \subject
  ;     \message
  Procedure CreateNetmailMsg(*msg.Message)
    columns.s = "from_name, "
    columns.s + "to_name, "
    columns.s + "from_address, "
    columns.s + "to_address, "
    columns.s + "date, "
    columns.s + "subject, "
    columns.s + "text, "
    columns.s + "route_via, "
    columns.s + "send, "
    columns.s + "attr, "
    columns.s + "last_modified "

    query.s = "INSERT INTO netmail ("+columns.s+") VALUES ("
    query.s + "'"+FixInvertedCommas(*msg\from_name)+"', "
    query.s + "'"+FixInvertedCommas(*msg\to_name)+"', "
    query.s + "'"+FixInvertedCommas(*msg\from_ftn_addr)+"', "
    query.s + "'"+FixInvertedCommas(*msg\to_ftn_addr)+"', "
    query.s + *msg\date + ", "
    query.s + "'"+FixInvertedCommas(*msg\subject)+"', "
    query.s + "'"+FixInvertedCommas(*msg\message)+"', "
    query.s + CFG::jNodeLinkID + ", "
    query.s + 0 + ", "
    query.s + 0 + ", "
    query.s + *msg\date + ");"
    
    If DatabaseUpdate(DatabaseID, query.s)
      result = 1
    Else
      DatabaseError.s = DatabaseError()
      Debug "DB::CreateNetmailMsg() > error > "+DatabaseError()
      result = 0
    EndIf
    
    ProcedureReturn result
  EndProcedure
  
  ; Создаёт сообщение в арии echomail.
  ; Для создания сообщения необходимо заполнить следующие элементы структуры:
  ; *msg\echoarea_id
  ;     \from_name
  ;     \to_name
  ;     \from_ftn_addr
  ;     \date
  ;     \subject
  ;     \message
  ;     \msgid
  Procedure CreateEchomailMsg(*msg.Message)

    ;{ Формируем запрос на добавление.
    columns.s = "echoarea_id, "
    columns.s + "from_name, "
    columns.s + "to_name, "
    columns.s + "from_ftn_addr, "
    columns.s + "date, "
    columns.s + "subject, "
    columns.s + "message, "
    columns.s + "msgid "

    query.s + "INSERT INTO echomail ("+columns.s+") VALUES ("
    query.s + *msg\echoarea_id+", "
    query.s + "'"+FixInvertedCommas(*msg\from_name)+"', "
    query.s + "'"+FixInvertedCommas(*msg\to_name)+"', "
    query.s + "'"+FixInvertedCommas(*msg\from_ftn_addr)+"', "
    query.s + *msg\date + ", "
    query.s + "'"+FixInvertedCommas(*msg\subject)+"', "
    query.s + "'"+FixInvertedCommas(*msg\message)+"', "
    query.s + "'"+FixInvertedCommas(*msg\msgid)+"'); "
    ;}
    
    If DatabaseUpdate(DatabaseID, query.s)
      ; Создаём упоминание о письме в echomailawait, чтобы jnode отправил его в сеть.
      If DatabaseUpdate(DatabaseID, "INSERT INTO echomailawait (link_id, echomail_id) VALUES ("+CFG::jNodeLinkID+", (Select LAST_INSERT_ROWID()));")
        result = 1
      Else
        result = 0
      EndIf
    Else
      result = 0
    EndIf
    
    If result = 0
      DatabaseError.s = DatabaseError()
      Debug "DB::CreateEchomailMsg() > error > "+DatabaseError()
    EndIf
    
    ProcedureReturn result
  EndProcedure

  ; Удалит #Netmail или #Echomail сообщение msg_id из БД.
  Procedure DeleteMessage(msg_type, msg_id, echoarea_id=0)
    If msg_type = #Netmail
      query.s="DELETE FROM netmail WHERE id="+msg_id+";"
    ElseIf msg_type = #Echomail
      query.s = "BEGIN TRANSACTION;"+Chr(10)
      query.s + "DELETE FROM echomail WHERE echoarea_id="+echoarea_id+" And id="+msg_id+";"+Chr(10)
      query.s + "DELETE FROM echomailawait WHERE echomail_id="+msg_id+";"+Chr(10)
      query.s + "COMMIT;"
    EndIf
    
    If query.s<>""
      If DatabaseUpdate(DatabaseID, query.s)
        result = 1
      Else
        DatabaseError.s = DatabaseError()
        Debug "DB::DeleteMessage() > error > "+DatabaseError()
        result = 0
      EndIf
      ProcedureReturn result
    EndIf
  EndProcedure
  
  ; Отметит сообщение прочитанным.
  ; area_type принимает значения #Netmail или #Echomail.
  Procedure MarkRead(area_type, msg_id, echoarea_id=0)
    If area_type = #Netmail
      query.s="UPDATE netmail SET read=1 WHERE id="+msg_id+";"
    ElseIf area_type = #Echomail
      query.s="UPDATE echomail SET read=1 WHERE echoarea_id="+echoarea_id+" AND id="+msg_id+";"
    EndIf

    If query.s<>""
      If DatabaseUpdate(DatabaseID, query.s)
        result = 1
      Else
        DatabaseError.s = DatabaseError()
        Debug "DB::MarkRead() > error > "+DatabaseError.s
        result = 0
      EndIf
      ProcedureReturn result
    EndIf
  EndProcedure
  
  ; Отметит сообщение непрочитанным.
  ; area_type принимает значения #Netmail или #Echomail.
  Procedure MarkUnread(area_type, msg_id, echoarea_id=0)
    If area_type = #Netmail
      query.s="UPDATE netmail SET read=0 WHERE id="+msg_id+";"
    ElseIf area_type = #Echomail
      query.s="UPDATE echomail SET read=0 WHERE echoarea_id="+echoarea_id+" AND id="+msg_id+";"
    EndIf
    
    If query.s<>""
      If DatabaseUpdate(DatabaseID, query.s)
        result = 1
      Else
        DatabaseError.s = DatabaseError()
        Debug "DB::MarkUnread() > error > "+DatabaseError.s
        result = 0
      EndIf
      ProcedureReturn result
    EndIf
  EndProcedure
  
  ; Отметит все сообщения в арии прочитанным.
  ; area_type принимает значения #Netmail или #Echomail.
  Procedure MarkAllRead(area_type, echoarea_id=0)
    If area_type = #Netmail
      query.s = "UPDATE netmail SET read=1;"
    ElseIf area_type = #Echomail
      query.s = "UPDATE echomail SET read=1 WHERE echoarea_id="+echoarea_id+";"
    EndIf

    If query.s<>""
      If DatabaseUpdate(DatabaseID, query.s)
        result = 1
      Else
        DatabaseError.s = DatabaseError()
        Debug "DB::MarkRead() > error > "+DatabaseError.s
        result = 0
      EndIf
      ProcedureReturn result
    EndIf
  EndProcedure
  
EndModule

; IDE Options = PureBasic 5.31 (Windows - x86)
; CursorPosition = 416
; FirstLine = 416
; Folding = ----
; EnableUnicode
; EnableXP