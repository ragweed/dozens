﻿DeclareModule Styling
  
  ;- [СТИЛЕВЫДЕЛЕНИЕ] Лексеры для скинтиллы.
  Enumeration 0 
    #LexerState_Space ; Пробелы и другие разделители
    #LexerState_Keyword ; Обычный текст
    
    #Lexer_Kludge
    #Lexer_Quote1
    #Lexer_Quote2
    #Lexer_Signature
  EndEnumeration
  
  Declare GetStyle(string.s, default_style)
  
EndDeclareModule

Module Styling
  
  ;-[regexp] Пытаюсь в регулярные выражения. 
  Procedure RegExp(pattern.s, test.s)
    Protected id, result
    id = CreateRegularExpression(#PB_Any, pattern.s)
    If id <> 0
      result = MatchRegularExpression(id, test.s)
      FreeRegularExpression(id)
    Else
      Debug "RegExp() " + RegularExpressionError()
    EndIf
    ProcedureReturn result
  EndProcedure
  
  ;-[regexp] Пытаюсь в регулярные выражения. 
  Procedure GetQuoteType(test.s)
    Protected id, result
    Dim TempArray.s(0)
    id = CreateRegularExpression(#PB_Any, ">+")
    If id <> 0
      If ExtractRegularExpression(id, test.s, TempArray())
        If (Len(TempArray(0)) % 2) <> 0
          ProcedureReturn #Lexer_Quote1
        Else
          ProcedureReturn #Lexer_Quote2
        EndIf
      EndIf  
      FreeRegularExpression(id)
    Else
      Debug "RegExp() " + RegularExpressionError()
    EndIf
  EndProcedure
  
  ; Вернёт лексер, которому соотвествует строка string.
  Procedure GetStyle(string.s, default_style)
    Protected result = default_style
    If RegExp("^@[A-Z]{1,}: ", string.s)
      result = #Lexer_Kludge
    ElseIf RegExp("^@Via ", string.s)
      result = #Lexer_Kludge
    ElseIf RegExp("^ \* Origin: ", string.s)
      result = #Lexer_Signature
    ElseIf RegExp("^--- ", string.s)
      result = #Lexer_Signature
    ElseIf RegExp("^\.\.\. ", string.s)
      result = #Lexer_Signature
    ElseIf RegExp("^[ ]{0,1}[\S]{1,4}>+ ", string.s)
      result = GetQuoteType(string.s)
    EndIf
    ProcedureReturn result
  EndProcedure  

EndModule
; IDE Options = PureBasic 5.31 (Windows - x86)
; CursorPosition = 19
; Folding = -
; EnableUnicode
; EnableXP