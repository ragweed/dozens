﻿
DeclareModule CFG
  
  Structure Signatures
    tagline.s
    tearline.s
    origin.s
  EndStructure
  
  Structure User
    Name.s
    Address.s
    BossName.s
    StationName.s
    Location.s
    BossAddress.s
    ServerName.s
    Password.s
  EndStructure
  
  Declare LoadConfig()
  Declare SaveConfig()
  
  ; Здесь будем хранить имя конфига.
  Global Path.s = "config.cfg"
  
  ; Здесь будем хранить путь до sqlite БД jNode.
  Global jNodeDB.s
  
  ; Здесь будем хранить id узла, значение которого будет указыватся при создании сообщений.
  Global jNodeLinkID
  
  ; Здесь будем хранить расположение скрипта, запускающего jNode для отправки почты.
  Global jNodeScriptSend.s
  
  ; А здесь будем хранить информацию о пользователе.
  Global User.User
  
  ; А здесь будем хранить ориджин, теарлайн и теглайн.
  Global Signatures.Signatures

EndDeclareModule

Module CFG
  
  ; Загружаем настройки из конфига.
  Procedure LoadConfig()
    
    If OpenPreferences(Path.s)
      open = 1
    EndIf
    
    PreferenceGroup("jNode")
    jNodeDB.s = ReadPreferenceString("DataBasePath", "jnode/fidomail.sqlite3")
    jNodeLinkID = ReadPreferenceInteger("LinkID", 1)

    CompilerSelect #PB_Compiler_OS
      CompilerCase #PB_OS_Windows
        jNodeScriptSend.s = ReadPreferenceString("ScriptSend", "jnode\bin\run.bat")
      CompilerDefault
        jNodeScriptSend.s = ReadPreferenceString("ScriptSend", "jnode/bin/run.sh")
    CompilerEndSelect
    
    PreferenceGroup("FTN")
    User\Name = ReadPreferenceString("UserName","")
    User\Address = ReadPreferenceString("UserFTN","")
    
    PreferenceGroup("Signatures")
    Signatures\tagline = ReadPreferenceString("Tagline","")
    Signatures\tearline = ReadPreferenceString("Tearline","Лучше б она коня на руках в горящую избу вносила...")
    Signatures\origin = ReadPreferenceString("Origin","Канпелятор в руки и вперёд!")
    
    PreferenceGroup("Window")
    With GUI::Window
      \x = ReadPreferenceLong("x", 50)
      \y = ReadPreferenceLong("y", 50)
      \w = ReadPreferenceLong("w", 800)
      \h = ReadPreferenceLong("h", 500)
      \state = ReadPreferenceLong("state", #PB_Window_Normal)
      
      \type = ReadPreferenceLong("type", GUI::#Classic)
      \msgs_mode = ReadPreferenceLong("msgs_mode", GUI::#List)
      \show_only_unread = ReadPreferenceLong("show_only_unread", 0)
      
      \splitter1 = ReadPreferenceLong("splitter1", 250)
      \splitter2 = ReadPreferenceLong("splitter2", 150)
    EndWith

    If open = 1
      ClosePreferences()
    EndIf

  EndProcedure
  
  ; Сохраняем текущие параметры в конфиг.
  Procedure SaveConfig()
    HideWindow(GUI::#Window_General, 1, #PB_Window_NoActivate)
    If OpenPreferences(Path.s)
      PreferenceGroup("Window")
        WritePreferenceLong("splitter1", GetGadgetState(GUI::#GW_Splitter_AreasAndMsgs))  
        If GUI::Window\type = GUI::#Classic
          WritePreferenceLong("splitter2", GetGadgetState(GUI::#GW_Splitter_ListAndWiew_H))
        Else
          WritePreferenceLong("splitter2", GetGadgetState(GUI::#GW_Splitter_ListAndWiew_V))
        EndIf
        WritePreferenceLong("state", GetWindowState(GUI::#Window_General))
        If GetWindowState(GUI::#Window_General) <> #PB_Window_Normal
          SetWindowState(GUI::#Window_General, #PB_Window_Normal)
        EndIf
        WritePreferenceLong("x", WindowX(GUI::#Window_General))
        WritePreferenceLong("y", WindowY(GUI::#Window_General))
        WritePreferenceLong("w", WindowWidth(GUI::#Window_General))
        WritePreferenceLong("h", WindowHeight(GUI::#Window_General))
        
        WritePreferenceLong("type", GUI::Window\type)
        WritePreferenceLong("msgs_mode", GUI::Window\msgs_mode)  
        WritePreferenceLong("show_only_unread", GUI::Window\show_only_unread)
      ClosePreferences()
    EndIf
  EndProcedure
  
EndModule
; IDE Options = PureBasic 5.31 (Windows - x86)
; CursorPosition = 59
; FirstLine = 46
; Folding = -
; EnableUnicode
; EnableXP