﻿; ########################
; Custom TreeView Gadget
; PureBasic 5.24LTS
; Windows, Linux, MacOS
; (w) 09-12/2015 by V. Schmid
; http://www.purebasic.fr/english/viewtopic.php?f=12&t=62368
; ########################

EnableExplicit
UsePNGImageDecoder()

DeclareModule TreeView

  #collapsed = 0
  #open = 1
  #parent_root = 0
  
  #flags_multiSelect = 1
  #flags_autoSize = 2
  
  Structure ct_customizing
    fontId.i                ; ID of the font to use
    foregroundColor.i
    backgroundColor.i
    selForegroundColor.i
    selBackgroundColor.i
    lineColor.i
    headerForegroundColor.i
    headerBackroundColor.i
  EndStructure
  
  Structure ct_column
    text.s          ; the column text content
    imageId.i       ; imageId to use (0 = no image)
    itemDataStr.s   ; individual item data
    itemDataPtr.i   ; individual item data
  EndStructure
  
  Structure ct_header
    text.s          ; column header text
    position.i      ; header position (x)
    width.i         ; header width
    maxWidth.i      ; maximum entry width
    itemDataStr.s   ; individual item data
    itemDataPtr.i   ; individual item data
  EndStructure
  
  Structure ct_entry
    text.s            ; entry text
    sortValue.s       ; dummy entry for column sort
    status.i          ; #collapsed / #open
    imageId.i         ; imageId to use (0 = no image)
    itemDataStr.s     ; individual item data
    itemDataPtr.i     ; individual item data
    selected.i        ; #True or #False if selected
    x.i               ; x-position in canvas or 0 (invisible)
    y.i               ; y-position in canvas or 0 (invisible)
    entry.i           ;- Ссылка на данный элемент в дереве
    *parent.ct_entry  ; pointer to parent node of type ct_entry
    List columns.ct_column() ; list op potential additional columns
    List childs.ct_entry()   ; list of possible tree child nodes
  EndStructure
  
  Structure ct_main
    x.i                     ; gadget x-position
    y.i                     ; gadget y-position
    width.i                 ; gadget width
    height.i                ; gadget height
    gadgetId.i              ; canvas gadget id
    hScrollId.i             ; hScroll gadget id
    vScrollId.i             ; vScroll gadget id
    scrollX.i               ; scroll position X (0=top)
    scrollY.i               ; scroll position Y (0=left)
    flags.i                 ; several flags
    treeHeight.i            ; internal tree height calculation
    treeWidth.i             ; internal tree width calculation
    lineHeight.i            ; height of one line regarding config (font)
    lineCount.i             ; number of lines (respecting collapsed as 1)
    headerCount.i           ; Number of used columns
    customization.ct_customizing ; optical customization data
    Array header.ct_header(50)   ; column information
    List childs.ct_entry()       ; list or root items (incl. childs)
  EndStructure
  
  Declare.i redraw(*tree.ct_main)
  Declare.i clear(*tree.ct_main)
  Declare.i create(x.i, y.i, width.i, height.i, flags.i = 0)
  Declare.i customize(*tree.ct_main, *config.ct_customizing)
  Declare.i addItem(*treeId.ct_main, *parent.ct_entry, text.s, imageId.i = 0, itemData.s = "")
  Declare sort(*treeId.ct_main, Mode.i = #PB_Sort_Ascending | #PB_Sort_NoCase)
  Declare sortColumn(*treeId.ct_main, columnId.i, Mode.i = #PB_Sort_Ascending | #PB_Sort_NoCase)
  Declare.i getSelected(*treeId.ct_main, List entries.ct_entry())
  Declare.i resizeHeader(*treeId.ct_main, columnId.i, width.i)
  Declare.i setIcon(*entry.ct_entry, columnId.i, imageId.i)
  Declare.i resizeTree(*treeId.ct_main, x.i, y.i, width.i, height.i)
  Declare.i mouseOnHeader(*treeId.ct_main, x.i = #PB_Ignore, y.i = #PB_Ignore)
  
  ;- Custom procedures.
  Declare clearAllItems(*treeId.ct_main)
  Declare.s getCurrentItemDataStr(*treeId.ct_main)
  Declare getCurrentEntry(*treeId.ct_main)
  Declare setState(*treeId.ct_main, *entry.ct_entry)
  Declare setIconAllItems(*treeId.ct_main, columnId.i, imageId.i)

EndDeclareModule

Module TreeView

  #tree_fx_icon_margin = 5        ; General margin between icons and text in tree
  #tree_fx_col_margin = 5         ; Margin for column header text (line to text)
  #tree_fx_sel_margin = 3         ; +- offset for selecting the column line with the mouse
  #tree_fx_linecolor = $AAAAAA
  #tree_fx_defaultColWidth = 150  ; default column width
  #tree_fx_indentFactor = 0.5     ; indent factor (<1 less, >1 more, best 1.5)
  #tree_fx_scrollBarSize = 15     ; scrollbar size
  #tree_fx_lineMargin = 3         ; left line margin
  
  ;{ Image data for expanded and collapsed image
  DataSection
  tree_expanded_image:
    Data.b $89,$50,$4E,$47,$0D,$0A,$1A,$0A,$00,$00,$00,$0D,$49,$48,$44,$52
    Data.b $00,$00,$00,$30,$00,$00,$00,$30,$08,$06,$00,$00,$00,$57,$02,$F9
    Data.b $87,$00,$00,$00,$07,$74,$49,$4D,$45,$07,$DF,$05,$05,$0C,$1D,$24
    Data.b $1A,$F7,$0A,$B1,$00,$00,$00,$09,$70,$48,$59,$73,$00,$00,$0B,$12
    Data.b $00,$00,$0B,$12,$01,$D2,$DD,$7E,$FC,$00,$00,$00,$04,$67,$41,$4D
    Data.b $41,$00,$00,$B1,$8F,$0B,$FC,$61,$05,$00,$00,$01,$F3,$49,$44,$41
    Data.b $54,$78,$DA,$ED,$99,$31,$6E,$C2,$30,$14,$86,$43,$09,$33,$DC,$00
    Data.b $65,$01,$16,$50,$60,$63,$6B,$6F,$D2,$0E,$9C,$00,$EE,$41,$77,$86
    Data.b $F6,$26,$ED,$09,$48,$45,$48,$44,$A6,$48,$9C,$00,$C1,$08,$52,$EA
    Data.b $1F,$51,$29,$3C,$1C,$12,$62,$07,$37,$C8,$9F,$64,$09,$99,$C4,$EF
    Data.b $FF,$9D,$67,$0F,$EF,$19,$86,$46,$A3,$79,$7C,$3C,$CF,$6B,$8E,$C7
    Data.b $E3,$69,$AB,$D5,$0A,$2B,$95,$4A,$C4,$A6,$A4,$0F,$AC,$8B,$F5,$11
    Data.b $07,$F1,$A4,$08,$DF,$6E,$B7,$F5,$D1,$68,$F4,$51,$AD,$56,$0B,$11
    Data.b $9D,$34,$10,$0F,$71,$11,$3F,$B7,$F8,$F5,$7A,$5D,$EF,$76,$BB,$CE
    Data.b $3D,$85,$D3,$D1,$EB,$F5,$1C,$E8,$C8,$B5,$F3,$AA,$C5,$C7,$4D,$5C
    Data.b $FB,$12,$26,$6F,$92,$E5,$E1,$BB,$EB,$BA,$36,$9D,$67,$79,$6A,$0C
    Data.b $06,$83,$9F,$76,$BB,$BD,$31,$4D,$D3,$90,$C5,$E1,$70,$30,$82,$20
    Data.b $68,$CC,$E7,$73,$3B,$8A,$A2,$B3,$FF,$16,$8B,$85,$0D,$3D,$EC,$E7
    Data.b $5B,$A6,$C5,$7C,$DF,$6F,$F2,$72,$7E,$38,$1C,$3A,$CB,$E5,$52,$CE
    Data.b $E1,$4A,$00,$EB,$23,$0E,$8D,$0D,$3D,$99,$0F,$F6,$64,$32,$99,$F2
    Data.b $C4,$17,$29,$9C,$C2,$33,$71,$D2,$95,$0E,$4B,$8F,$30,$FE,$22,$AE
    Data.b $B7,$A2,$77,$9E,$82,$78,$F4,$BA,$3E,$E9,$4A,$87,$BE,$C8,$72,$FE
    Data.b $AE,$BB,$FF,$C7,$29,$EE,$D9,$46,$F2,$9E,$7B,$A2,$13,$F4,$10,$75
    Data.b $3A,$9D,$8D,$0A,$03,$34,$2E,$D5,$95,$68,$80,$22,$F3,$B6,$B9,$85
    Data.b $AC,$71,$53,$0D,$FC,$77,$B4,$01,$D5,$68,$03,$AA,$29,$BD,$01,$A1
    Data.b $3B,$D2,$B2,$AC,$2F,$51,$01,$61,$18,$BE,$28,$33,$C0,$82,$3F,$8B
    Data.b $1A,$10,$A5,$F4,$29,$54,$7A,$03,$A2,$67,$E0,$5B,$54,$00,$4B,$43
    Data.b $75,$06,$44,$0F,$A0,$0C,$4A,$9F,$42,$DA,$80,$6A,$B4,$01,$D5,$3C
    Data.b $BE,$01,$14,$9D,$54,$90,$35,$EE,$85,$01,$54,$DF,$E2,$AC,$56,$AB
    Data.b $86,$0A,$03,$34,$2E,$D5,$95,$48,$E9,$EB,$42,$A5,$AF,$CC,$5D,$AB
    Data.b $8D,$BA,$AE,$5B,$E8,$97,$C0,$FA,$49,$B5,$51,$E8,$E2,$BD,$C3,$4D
    Data.b $2C,$34,$17,$66,$B3,$D9,$EB,$C5,$C3,$2C,$0F,$FB,$FD,$FE,$B1,$3A
    Data.b $5D,$AB,$D5,$A4,$09,$DF,$EF,$F7,$C7,$EA,$B4,$E3,$38,$36,$AF,$80
    Data.b $C5,$F4,$7C,$32,$3D,$D9,$AA,$D3,$00,$F5,$78,$D4,$E5,$8D,$1B,$6B
    Data.b $F9,$45,$0C,$E8,$D8,$ED,$76,$B7,$37,$39,$D0,$19,$51,$6D,$22,$77
    Data.b $87,$26,$FE,$25,$54,$F6,$C8,$72,$ED,$3C,$0F,$34,$17,$70,$0B,$E0
    Data.b $2A,$2B,$B2,$4B,$89,$F5,$11,$27,$E9,$C0,$6A,$34,$9A,$4B,$7E,$01
    Data.b $17,$BF,$F8,$D8,$06,$73,$FD,$50,$00,$00,$00,$00,$49,$45,$4E,$44
    Data.b $AE,$42,$60,$82
  EndDataSection
  
  DataSection
  tree_collapsed_image:
    Data.b $89,$50,$4E,$47,$0D,$0A,$1A,$0A,$00,$00,$00,$0D,$49,$48,$44,$52
    Data.b $00,$00,$00,$30,$00,$00,$00,$30,$08,$06,$00,$00,$00,$57,$02,$F9
    Data.b $87,$00,$00,$00,$07,$74,$49,$4D,$45,$07,$DF,$05,$05,$0C,$1D,$15
    Data.b $4B,$29,$0A,$8B,$00,$00,$00,$09,$70,$48,$59,$73,$00,$00,$0B,$12
    Data.b $00,$00,$0B,$12,$01,$D2,$DD,$7E,$FC,$00,$00,$00,$04,$67,$41,$4D
    Data.b $41,$00,$00,$B1,$8F,$0B,$FC,$61,$05,$00,$00,$02,$CB,$49,$44,$41
    Data.b $54,$78,$DA,$ED,$59,$DD,$8A,$DA,$40,$14,$8E,$55,$8B,$15,$B2,$BB
    Data.b $ED,$9D,$20,$65,$C5,$AD,$3F,$68,$89,$8A,$8A,$D2,$42,$2F,$FA,$04
    Data.b $FB,$12,$2A,$88,$77,$A2,$82,$E0,$4B,$78,$E3,$8B,$14,$EC,$DD,$B2
    Data.b $F6,$46,$11,$53,$BD,$50,$8C,$55,$A1,$04,$41,$A1,$60,$D5,$AE,$4D
    Data.b $FD,$EB,$4C,$21,$45,$E3,$A8,$31,$1A,$53,$B7,$F9,$20,$A0,$33,$C9
    Data.b $CC,$F7,$CD,$9C,$39,$33,$73,$0E,$86,$C9,$90,$21,$E3,$F1,$83,$A2
    Data.b $28,$22,$16,$8B,$DD,$9B,$CD,$66,$46,$A1,$50,$2C,$40,$D1,$D1,$1F
    Data.b $D8,$AE,$C9,$64,$62,$A2,$D1,$28,$59,$AD,$56,$DF,$1D,$85,$F8,$70
    Data.b $38,$BC,$0A,$87,$C3,$9F,$95,$4A,$A5,$28,$A4,$37,$3D,$B0,$BF,$40
    Data.b $20,$50,$1B,$0C,$06,$57,$82,$C9,$F7,$7A,$BD,$6B,$A7,$D3,$D9,$3F
    Data.b $25,$71,$EE,$43,$10,$C4,$03,$4D,$D3,$D7,$7B,$93,$07,$CA,$2F,$01
    Data.b $F9,$6F,$52,$92,$5F,$16,$B1,$6D,$26,$54,$A8,$C2,$64,$32,$F9,$81
    Data.b $24,$C9,$17,$DC,$72,$60,$A7,$98,$C3,$E1,$60,$2C,$16,$CB,$58,$AD
    Data.b $56,$0B,$9E,$5D,$2E,$A6,$D3,$29,$56,$AF,$D7,$9F,$96,$4A,$25,$ED
    Data.b $62,$B1,$58,$A9,$2B,$97,$CB,$CF,$C0,$FA,$BB,$03,$3F,$1D,$BC,$1A
    Data.b $6B,$B5,$5A,$AF,$51,$36,$EF,$F5,$7A,$7F,$16,$0A,$85,$DB,$A3,$B1
    Data.b $46,$A0,$52,$A9,$BC,$F7,$FB,$FD,$3F,$B8,$7D,$43,$3E,$C0,$91,$BC
    Data.b $E5,$D5,$48,$2A,$95,$FA,$C8,$6D,$C0,$E3,$F1,$FC,$EA,$76,$BB,$B8
    Data.b $98,$E4,$59,$8C,$46,$23,$DC,$E7,$F3,$8D,$B8,$1C,$12,$89,$C4,$1D
    Data.b $AF,$06,$6C,$36,$DB,$CA,$08,$40,$F7,$96,$CD,$66,$C3,$A7,$20,$CF
    Data.b $02,$98,$D2,$2D,$D7,$5D,$5B,$AD,$D6,$31,$AF,$8F,$B9,$1F,$DA,$ED
    Data.b $F6,$D9,$29,$C9,$B3,$00,$4E,$64,$8C,$71,$06,$12,$F5,$DE,$13,$6E
    Data.b $01,$77,$11,$81,$CD,$A5,$2F,$85,$00,$B0,$69,$3E,$6C,$E3,$B5,$51
    Data.b $00,$17,$2A,$95,$6A,$D7,$2B,$A2,$80,$6F,$BF,$3B,$05,$08,$81,$DB
    Data.b $ED,$6E,$60,$4B,$D3,$AF,$D7,$EB,$1B,$62,$09,$15,$45,$C0,$29,$21
    Data.b $0B,$90,$1A,$B2,$00,$A9,$21,$D8,$47,$82,$BB,$02,$0E,$0E,$76,$5F
    Data.b $E7,$F3,$F9,$5A,$5D,$A7,$D3,$B9,$D0,$E9,$74,$7F,$FF,$CF,$66,$B3
    Data.b $1B,$83,$C1,$C0,$1E,$CD,$57,$90,$CB,$E5,$5E,$02,$2F,$35,$3C,$B9
    Data.b $00,$B0,$B1,$A8,$9A,$CD,$E6,$3E,$17,$8E,$4B,$54,$E1,$64,$32,$39
    Data.b $68,$A3,$39,$7B,$13,$3A,$7B,$01,$82,$A7,$0F,$1C,$AE,$A6,$46,$A3
    Data.b $B1,$8F,$5A,$03,$0C,$C3,$5C,$00,$13,$5B,$19,$1C,$8D,$46,$F3,$1D
    Data.b $43,$AC,$01,$70,$31,$9A,$4A,$22,$00,$C7,$71,$B8,$F0,$9E,$A3,$EA
    Data.b $E0,$51,$A2,$58,$2C,$DE,$B0,$FF,$C1,$22,$FD,$D2,$6E,$B7,$5F,$A1
    Data.b $DE,$05,$75,$87,$F0,$3F,$7F,$13,$92,$05,$48,$0D,$59,$80,$D4,$10
    Data.b $E5,$BA,$05,$3C,$D0,$8A,$C7,$A1,$69,$5A,$34,$01,$3B,$67,$00,$06
    Data.b $9D,$A4,$00,$DF,$7E,$D7,$04,$C0,$E8,$DB,$32,$28,$8A,$12,$1E,$60
    Data.b $3D,$00,$F5,$7A,$5D,$BB,$8D,$D7,$46,$9C,$7D,$5C,$E8,$EC,$23,$73
    Data.b $DB,$62,$A3,$F9,$7C,$5E,$B2,$D8,$68,$A3,$D1,$40,$C6,$46,$91,$86
    Data.b $15,$89,$44,$3E,$A5,$D3,$E9,$37,$6B,$2F,$03,$3B,$24,$08,$82,$81
    Data.b $D3,$79,$CC,$E8,$34,$B8,$13,$FC,$89,$4E,$93,$24,$A9,$45,$05,$B0
    Data.b $42,$A1,$50,$39,$93,$C9,$F0,$8B,$4E,$43,$C0,$FC,$80,$CB,$E5,$FA
    Data.b $67,$F2,$03,$30,$53,$B4,$F7,$A8,$C0,$0C,$0D,$10,$71,$9E,$19,$1A
    Data.b $16,$52,$E6,$C8,$82,$C1,$60,$4D,$D0,$C8,$A3,$00,$B3,$94,$F1,$78
    Data.b $FC,$DE,$62,$B1,$88,$9A,$A5,$84,$59,$50,$98,$A5,$AC,$D5,$6A,$C7
    Data.b $C9,$52,$CA,$90,$F1,$1F,$E0,$37,$26,$2B,$3E,$6C,$9A,$12,$67,$96
    Data.b $00,$00,$00,$00,$49,$45,$4E,$44,$AE,$42,$60,$82
  EndDataSection
  ;}
  

  ; Determine the system DPI factor to scale the content with.
  ; Returns a float value that is used to multiply the buttons dimensions with.
  ; Windows: GetDeviceCaps()
  ; Linux: gdk_screen_get_resolution()
  ; MacOS: Cocoa deviceDescription/NSDeviceResolution
  Procedure.f _cust_tree_DetermineSystemDPIFactor()
    Static LastFactor.f = 0
    If LastFactor.f = 0
      LastFactor.f = 1 ; init
      CompilerSelect #PB_Compiler_OS
        CompilerCase #PB_OS_Windows
          ; get DPI on Windows (default 96 DPI)
          Protected hDC.i  = GetWindowDC_(GetDesktopWindow_())
          Protected lDPI.i = GetDeviceCaps_(hDC.i, #LOGPIXELSX ) ; DPI
          ReleaseDC_(GetDesktopWindow_(), hDC.i)
          If lDPI.i > 0
            LastFactor.f = lDPI / 96; 96 DPI is Windows default for 100%
          EndIf
        CompilerCase #PB_OS_MacOS
          ; Get DPI on Mac (default 72 DPI)
          Protected dpi.NSSize
          Protected screen.i = CocoaMessage(0, 0, "NSScreen mainScreen")
          Protected description.i = CocoaMessage(0, screen, "deviceDescription")
          CocoaMessage(@dpi, CocoaMessage(0, description, "objectForKey:$", @"NSDeviceResolution"), "sizeValue")
          If dpi\width > 0
            LastFactor.f = dpi\width / 72; 72 DPI is MacOS default for 100%
          EndIf
        CompilerCase #PB_OS_Linux
            LastFactor.f = 1
         
      CompilerEndSelect
    EndIf
    ProcedureReturn LastFactor.f
  EndProcedure
  
  ; Makes the cursor in the window (parent of given Gadget) to a hand
  ; Works both windows and linux
  Procedure.b _cust_tree_cursorSplit(GadgetID.i, Standard.i = #False)
    CompilerSelect #PB_Compiler_OS
    CompilerCase #PB_OS_Windows
     
      ; WINDOWS
      Static *cursor
      If *cursor = 0
        *cursor = LoadCursor_(0, #IDC_SIZEWE)
      EndIf
      SetCursor_(*cursor)
     
    CompilerCase #PB_OS_Linux
     
      ; LINUX
      If Standard.i = #False
        Static *cursor.GdkCursor
        If *cursor = 0
          *cursor = gdk_cursor_new_(#GDK_SB_H_DOUBLE_ARROW);
        EndIf
      Else
        *cursor = #Null
      EndIf
      gdk_window_set_cursor_(gtk_widget_get_parent_window_(GadgetID(GadgetID.i)), *cursor)
     
    CompilerCase #PB_OS_MacOS
      ; MAC
      ; write me!
     
    CompilerEndSelect
  EndProcedure
  
  ; Determine the needed pixel height for a given fontId
  Procedure.i _cust_tree_getLineHeight(fontId.i)
    Protected i.i = CreateImage(#PB_Any, 50, 50)
    StartDrawing(ImageOutput(i.i))
    DrawingFont(FontID(fontId.i))
    Protected h.i = TextHeight("|´_") + 2
    StopDrawing()
    FreeImage(i.i)
    ProcedureReturn h.i
  EndProcedure
  
  Global NewList cust_trees.ct_main()
  Global ct_image_collapsed.i = CatchImage(#PB_Any, ?tree_collapsed_image)
  Global ct_image_expanded.i  = CatchImage(#PB_Any, ?tree_expanded_image)
  
  ; go through all the items to find out which is covered by given coordinates (eg click or hover)
  Procedure.i _cust_tree_getItem(*tree.ct_main, List tree.ct_entry(), x.i, y.i)
    Protected *Res.ct_entry
    ForEach tree()
      If y.i >= tree()\y And y.i <= tree()\y + *tree\lineHeight And tree()\y <> -1 And tree()\x <> -1
        ProcedureReturn tree()
      EndIf
      If ListSize(tree()\childs()) > 0
        *Res = _cust_tree_getItem(*tree, tree()\childs(), x.i, y.i)
        If *Res <> 0: Break: EndIf
      EndIf
    Next
    ProcedureReturn *Res
  EndProcedure
  
  Procedure.i _cust_tree_redraw_header(*main.ct_main)
    Protected c.i
    Protected textWidth.i = 0
    Protected widthOffset.i = 0
    Protected left.i = *main\scrollX
    With *main
      BackColor(\customization\headerBackroundColor)
      For c.i = 0 To \headerCount
        Box(\header(c.i)\position + 1 + left.i,
            0, 
            \header(c.i)\width - 1, 
            \lineHeight, 
            \customization\headerBackroundColor)
        textWidth = DrawText(\header(c.i)\position + #tree_fx_col_margin + left.i, 
                             0, 
                             \header(c.i)\text, 
                             \customization\headerForegroundColor)
        Line(\header(c.i)\position + \header(c.i)\width + left.i, 0, 1, \height, \customization\lineColor)
        textWidth.i = textWidth.i - widthOffset.i
        If *main\header(c.i)\maxWidth < textWidth
          *main\header(c.i)\maxWidth = textWidth + #tree_fx_col_margin
        EndIf
        widthOffset.i = widthOffset.i + *main\header(c.i)\width
      Next
      Protected rightMostX.i = \header(\headerCount)\position + \header(\headerCount)\width + 1
      Protected rightFillW.i = \width - rightMostX.i - left.i
      Box(rightMostX.i + left.i, 0, rightFillW.i, \lineHeight, \customization\headerBackroundColor)
      ; blank rest of column
      Box(rightMostX.i + left.i, \lineHeight + 1, rightFillW.i, \height, \customization\backgroundColor)
      ; horizontal line
      Line(0, \lineHeight, \width, 1, \customization\lineColor)
      \treeWidth = rightMostX.i + 1
    EndWith
  EndProcedure
  
  ; draw (optional) additional columns behind a tree entry
  Procedure.i _cust_tree_redraw_columns(*main.ct_main, List columns.ct_column(), *entry.ct_entry)
    Protected textWidth.i = 0
    Protected left.i = *main\scrollX
    Protected y.i = *main\treeHeight - *main\scrollY
   
    With *main
   
    If \headerCount < 1
      ProcedureReturn ; no additional columns
    EndIf
   
    DrawingMode(#PB_2DDrawing_Default)
   
    ; clean background
    If *entry\selected = #True
      Box(\header(1)\position + left.i - #tree_fx_lineMargin, 
          y.i, 
          \width + #tree_fx_lineMargin, 
          \lineHeight, 
          \customization\selBackgroundColor)
    Else
      Box(\header(1)\position + left.i - #tree_fx_lineMargin, 
          y.i, 
          \width + #tree_fx_lineMargin, 
          \lineHeight, 
          \customization\backgroundColor)
    EndIf
   
    If ListSize(columns()) < 1
      ; just clean (no real column data in this row available)
      ProcedureReturn
    EndIf
   
    ; draw real columns
    Protected c.i = 1
    Protected offX.i = 0
    Protected widthOffset.i = \header(0)\width
    ForEach columns()
      offX.i = left.i
      If IsImage(columns()\imageId)
        offX.i = *main\lineHeight + #tree_fx_icon_margin + left.i
      EndIf
      If *entry\selected = #True
        ; selected
        Box(\header(c.i)\position + left.i - #tree_fx_lineMargin,
            y.i,
            \header(c.i)\width + #tree_fx_lineMargin,
            \lineHeight,
            \customization\selBackgroundColor)
        textWidth.i = DrawText(\header(c.i)\position + #tree_fx_col_margin + offX.i,
                               y.i,
                               columns()\text,
                               \customization\selForegroundColor,
                               \customization\selBackgroundColor)
      Else
        ; not selected
        Box(\header(c.i)\position + left.i - #tree_fx_lineMargin,
            y.i,
            \header(c.i)\width + #tree_fx_lineMargin,
            \lineHeight,
            \customization\backgroundColor)
        textWidth.i = DrawText(\header(c.i)\position + #tree_fx_col_margin + offX.i,
                               y.i,
                               columns()\text,
                               \customization\foregroundColor,
                               \customization\backgroundColor)
      EndIf
      If IsImage(columns()\imageId)
        ; draw optional image
        DrawingMode(#PB_2DDrawing_AlphaBlend)
        DrawImage(ImageID(columns()\imageId), 
                  \header(c.i)\position + #tree_fx_col_margin + left.i, 
                  y.i, 
                  \lineHeight, 
                  \lineHeight)
        DrawingMode(#PB_2DDrawing_Default)
      EndIf
      textWidth.i = textWidth.i - widthOffset.i
      If textWidth > \header(c.i)\maxWidth
        \header(c.i)\maxWidth = textWidth + #tree_fx_col_margin
      EndIf
      widthOffset.i = widthOffset.i + \header(c.i)\width
      c.i = c.i + 1
    Next
   
    EndWith
  EndProcedure
  
  ; redraw the content of a custom tree node (recursively calling child nodes)
  Procedure.i _cust_tree_redraw_item(List tree.ct_entry(), level.i, *main.ct_main)
    Static headerHeight.i = 0
    Protected textWidth.i = 0
   
    If level.i = 0
      ; let's see what is needed
    EndIf
   
    With *main
   
    Protected indentWidth.i = \lineHeight * #tree_fx_indentFactor + #tree_fx_icon_margin
   
    ForEach tree()
      Protected xEntry.i = level.i * indentWidth.i + \scrollX ; x start
      Protected yEntry.i = \treeHeight - \scrollY  ; y start
      Protected imgO.i = 0
      If yEntry >= -*main\lineHeight And yEntry < *main\height
        ; visible entry (need to draw)
        If tree()\selected = #True
          Box(0, yEntry.i,
              \header(0)\width,
              \lineHeight,
              \customization\selBackgroundColor)
        EndIf
       
        tree()\x = xEntry.i
        tree()\y = yEntry.i
       
        DrawingMode(#PB_2DDrawing_AlphaBlend)
        If ListSize(tree()\childs()) > 0
          ; draw open/close images
          If tree()\status = #open
            DrawImage(ImageID(ct_image_expanded),
                      xEntry.i,
                      yEntry.i,
                      \lineHeight,
                      \lineHeight)
          Else
            DrawImage(ImageID(ct_image_collapsed),
                      xEntry.i,
                      yEntry.i,
                      \lineHeight,
                      \lineHeight)
          EndIf
          xEntry.i = xEntry.i + \lineHeight + #tree_fx_icon_margin
        Else
          xEntry.i = xEntry.i + #tree_fx_icon_margin
        EndIf
       
        If tree()\imageId <> 0
          ; draw entry image
          imgO.i = *main\lineHeight + #tree_fx_icon_margin
          DrawImage(ImageID(tree()\imageId),
                    xEntry.i,
                    yEntry.i,
                    \lineHeight,
                    \lineHeight)
          xEntry.i = xEntry.i + \lineHeight + #tree_fx_icon_margin
        EndIf
       
        DrawingMode(#PB_2DDrawing_Default)
        ; draw node text
        If tree()\selected = #True
          textWidth = DrawText(xEntry.i,
                               yEntry.i,
                               tree()\text,
                               \customization\selForegroundColor,
                               \customization\selBackgroundColor)
        Else
          textWidth = DrawText(xEntry.i,
                               yEntry.i,
                               tree()\text,
                               \customization\foregroundColor,
                               \customization\backgroundColor)
        EndIf
       
        If textWidth > *main\header(0)\maxWidth
          *main\header(0)\maxWidth = textWidth + #tree_fx_lineMargin
        EndIf
       
        ; draw additional columns
        _cust_tree_redraw_columns(*main, tree()\columns(), tree())
      Else
        ; invisible entry
        tree()\x = -1
        tree()\y = -1
      EndIf
     
      \treeHeight = \treeHeight + \lineHeight
      \lineCount = \lineCount + 1
     
      If ListSize(tree()\childs()) > 0
        If tree()\status = #open
          ; draw entries
          _cust_tree_redraw_item(tree()\childs(), level.i + 1, *main)
          ; draw line
          Protected lineX.i = level.i * indentWidth.i + \lineHeight / 2 + \scrollX
          Protected lineH.i = \treeHeight - yEntry.i - \scrollY - \lineHeight * 1.5
          Line(lineX.i,
               yEntry.i + \lineHeight,
               1,
               lineH.i,
               #tree_fx_linecolor)
          Line(lineX.i,
               yEntry.i + \lineHeight + lineH.i,
               \lineHeight / 2,
               1,
               #tree_fx_linecolor)
        Else
          ForEach tree()\childs()
            tree()\childs()\x = -1
            tree()\childs()\y = -1
          Next
        EndIf
      EndIf
    Next
    If level = 0
      ; finalize drawing
      If *main\headerCount > 0
        _cust_tree_redraw_header(*main)
      EndIf
    EndIf
   
    EndWith
   
  EndProcedure
  
  ; redraw the given custom tree
  Procedure.i redraw(*tree.ct_main)
    ; ensure correct column size for first column
    Protected x.i, colWidthCheckSum.i = 0
    If *tree\headerCount < 1
      ; set maximum width of first column to tree width
      *tree\header(0)\width = *tree\width
      ; no header to display
      *tree\treeHeight = 0
    Else
      ; set and verify column sizes for auto-sizing
      If Bool(*tree\flags & #flags_autoSize)
        For x.i = 0 To *tree\headerCount
          If *tree\header(x.i)\maxWidth > 0
            *tree\header(x.i)\width = *tree\header(x.i)\maxWidth + #tree_fx_lineMargin
            If x.i > 0
              *tree\header(x.i)\position = *tree\header(x.i-1)\position + *tree\header(x.i-1)\width
            EndIf
            colWidthCheckSum.i = colWidthCheckSum.i + *tree\header(x.i)\maxWidth
          EndIf
        Next
      EndIf
      ; display some header
      *tree\treeHeight = *tree\lineHeight
    EndIf
    ; start redrawing
    *tree\treeWidth = 0
    *tree\lineCount = 0
    StartDrawing(CanvasOutput(*tree\gadgetId))
    If IsFont(*tree\customization\fontId)
      DrawingFont(FontID(*tree\customization\fontId))
    EndIf
    With *tree
      ; clean area
      Box(0,0, \width, \height, \customization\backgroundColor)
      ; draw root items and their childs
      _cust_tree_redraw_item(\childs(), 0, *tree)
    EndWith
    StopDrawing()
   
    ; check if redraw is needed (because of auto-size)
    Protected colWidthCheckSum2.i = 0
    If Bool(*tree\flags & #flags_autoSize)
      For x.i = 0 To *tree\headerCount
        If *tree\header(x.i)\maxWidth > 0
          *tree\header(x.i)\width = *tree\header(x.i)\maxWidth
          If x.i > 0
            *tree\header(x.i)\position = *tree\header(x.i-1)\position + *tree\header(x.i-1)\width
          EndIf
          colWidthCheckSum2.i = colWidthCheckSum2.i + *tree\header(x.i)\maxWidth
        EndIf
      Next
      If colWidthCheckSum.i <> colWidthCheckSum2.i
        ; Width was changed during last draw.
        ; Redraw needed to adapt the new sizes.
        redraw(*tree)
      EndIf
    EndIf
   
    ; update scrollbar gadget(s) position (now I know the maximum height)
   
    ; update vertical scrollbar
    SetGadgetAttribute(*tree\vScrollId, #PB_ScrollBar_Maximum, *tree\lineCount)
    SetGadgetAttribute(*tree\vScrollId, #PB_ScrollBar_Minimum, 0)
    SetGadgetAttribute(*tree\vScrollId, #PB_ScrollBar_PageLength, Round(*tree\height / *tree\lineHeight, #PB_Round_Down)-1)
   
    ; update horizontal scrollbar
    Protected scrollWidth = *tree\treeWidth - *tree\width + #tree_fx_scrollBarSize
   
    scrollWidth = (1 + Round(scrollWidth / 20, #PB_Round_Up)) * 20
   
    SetGadgetAttribute(*tree\hScrollId, #PB_ScrollBar_Maximum, scrollWidth)
    SetGadgetAttribute(*tree\hScrollId, #PB_ScrollBar_Minimum, 0)
    SetGadgetAttribute(*tree\hScrollId, #PB_ScrollBar_PageLength, 20)
  EndProcedure
  
  Procedure.i clear(*tree.ct_main)
    ClearList(*tree\childs())
    Protected x.i
    For x.i = 0 To *tree\headerCount
      *tree\header(x.i)\itemDataPtr = 0
      *tree\header(x.i)\itemDataStr = ""
      *tree\header(x.i)\maxWidth = 0
      *tree\header(x.i)\text = "Column " + Str(c.i)
      *tree\header(x.i)\width = #tree_fx_defaultColWidth
      *tree\header(x.i)\position = x.i * #tree_fx_defaultColWidth
    Next
    *tree\header(0)\text = ""
    
    SetGadgetState(*tree\hScrollId, 0)
    SetGadgetState(*tree\vScrollId, 0)
    *tree\scrollX = 0
    *tree\scrollY = 0
  EndProcedure
  
  ; remove all selections recursively
  Procedure _cust_tree_reset_selection(List tree.ct_entry())
    ForEach tree()
      tree()\selected = #False
      If ListSize(tree()\childs()) > 0
        _cust_tree_reset_selection(tree()\childs())
      EndIf
    Next
  EndProcedure
  
  ; Check if coordinates are above a column resizer.
  ; Returns -1 if not, otherwise the column id to resize.
  Procedure.i _cust_tree_checkColumnSizer(*main.ct_main, x.i, y.i)
    If y.i > *main\lineHeight Or *main\headerCount < 1
      ; definitely no column sizer!
      ProcedureReturn -1
    EndIf
   
    If Bool(*main\flags & #flags_autoSize)
      ; no column sizer in auto-size mode
      ProcedureReturn -1
    EndIf
   
    Protected p.i = 0
    Protected c.i = 0
    For c.i = 0 To *main\headerCount
      p.i = *main\header(c.i)\position + *main\header(c.i)\width + *main\scrollX
      If Abs(x.i - p.i) < #tree_fx_sel_margin
        ProcedureReturn c.i
      EndIf
    Next
   
    ProcedureReturn -1
  EndProcedure
  
  ; event handler for canvas gadget events
  Procedure.i _cust_tree_eventHandler()
    Static selColumnId.i = 0
    Static resizing.b = #False
    Static resizingCol.i = -1
    Static cursorIsChanged.i = #False
   
    Protected gadId.i = EventGadget()
    Protected gadEvt.i = EventType()
    Protected *tree.ct_main = 0
   
    ForEach cust_trees()
      If cust_trees()\gadgetId = gadId.i
        *tree = cust_trees()
        Break
      EndIf
    Next
    If *tree = 0: ProcedureReturn: EndIf
   
    ; handle events
    If gadEvt.i = #PB_EventType_LeftClick
      ; get clicked entry
      Protected *Selected.ct_entry = _cust_tree_getItem(*tree, *tree\childs(),
                                      GetGadgetAttribute(gadId.i, #PB_Canvas_MouseX),
                                      GetGadgetAttribute(gadId.i, #PB_Canvas_MouseY))
      If *Selected <> 0
        ; some entry was clicked
        If GetGadgetAttribute(gadId.i, #PB_Canvas_Modifiers) = #PB_Canvas_Control And
          Bool(*tree\flags & #flags_multiSelect)
          ; CTRL
          *Selected\selected = Bool(Not *Selected\selected)
        ElseIf GetGadgetAttribute(gadId.i, #PB_Canvas_Modifiers) = #PB_Canvas_Shift And
          Bool(*tree\flags & #flags_multiSelect)
          ; SHIFT (not supported as usual)
          *Selected\selected = Bool(Not *Selected\selected)
        Else
          ; nothing, ordinary click
          _cust_tree_reset_selection(*tree\childs())
          *Selected\selected = #True
        EndIf
       
        If ListSize(*Selected\childs()) > 0 And
           GetGadgetAttribute(gadId.i, #PB_Canvas_MouseX) < *Selected\x + *tree\lineHeight
           
          If *Selected\status = #open
            *Selected\status = #collapsed
          Else
            *Selected\status = #open
          EndIf
        EndIf
      EndIf
      ; redraw tree
      redraw(*tree)
    EndIf
   
    If gadEvt.i = #PB_EventType_MouseMove
      selColumnId.i = _cust_tree_checkColumnSizer(*tree,
                          GetGadgetAttribute(gadId.i, #PB_Canvas_MouseX),
                          GetGadgetAttribute(gadId.i, #PB_Canvas_MouseY))
                         
      If selColumnId.i > -1
        _cust_tree_cursorSplit(gadId.i)
        cursorIsChanged.i = #True
      Else
        If cursorIsChanged.i = #True
          _cust_tree_cursorSplit(gadId.i, #True)
          cursorIsChanged.i = #False
        EndIf
      EndIf
     
      Protected c.i
      If resizing.b = #True And resizingCol.i > -1
        ; move selected column width
        *tree\header(resizingCol)\width = GetGadgetAttribute(gadId.i, #PB_Canvas_MouseX) - *tree\header(resizingCol)\position - *tree\scrollX
        If *tree\header(resizingCol)\width < *tree\lineHeight
          *tree\header(resizingCol)\width = *tree\lineHeight
        EndIf
        ; re-pos the other columns
        For c.i = resizingCol+1 To *tree\headerCount
          *tree\header(c.i)\position = *tree\header(c.i-1)\position + *tree\header(c.i-1)\width
        Next
        ; redraw tree
        redraw(*tree)
      EndIf
    EndIf
   
    If gadEvt.i = #PB_EventType_LeftButtonDown And selColumnId.i > -1
      resizing.b = #True
      resizingCol.i = selColumnId.i
    EndIf
   
    If gadEvt.i = #PB_EventType_LeftButtonUp
      resizing.b = #False
      resizingCol.i = -1
    EndIf
   
    If gadEvt.i = #PB_EventType_MouseWheel
      ; Mouse wheel scrolling
      *tree\scrollY = *tree\scrollY - (GetGadgetAttribute(gadId.i, #PB_Canvas_WheelDelta) * *tree\lineHeight)
      If *tree\scrollY > *tree\treeHeight - *tree\height + #tree_fx_scrollBarSize
        *tree\scrollY = *tree\treeHeight - *tree\height + #tree_fx_scrollBarSize
      EndIf
      If *tree\scrollY < 0: *tree\scrollY = 0: EndIf
      SetGadgetState(*tree\vScrollId, *tree\scrollY / *tree\lineHeight)
      ; redraw tree
      redraw(*tree) 
    EndIf
   
    If gadEvt.i = #PB_EventType_SizeItem
      Debug "Size event!"
      *tree\width = GadgetWidth(gadId) + #tree_fx_scrollBarSize
      *tree\height = GadgetHeight(gadId) + #tree_fx_scrollBarSize
      ; redraw tree
      redraw(*tree) 
    EndIf
  EndProcedure
  
  ; event handler for scroll gadget events
  Procedure.i _cust_tree_scrollEventHandler()
    Protected gadId.i = EventGadget()
    Protected gadEvt.i = EventType()
    Protected *tree.ct_main = 0
    ForEach cust_trees()
      If cust_trees()\vScrollId = gadId.i Or
         cust_trees()\hScrollId = gadId.i
          *tree = cust_trees()
          Break
      EndIf
    Next
    If *tree = 0: ProcedureReturn: EndIf
   
    If gadId.i = *tree\vScrollId
      *tree\scrollY = GetGadgetState(gadId.i) * *tree\lineHeight
    EndIf
    If gadId.i = *tree\hScrollId
      *tree\scrollX = -GetGadgetState(gadId.i)
    EndIf
   
    If gadEvt.i = #PB_EventType_LeftClick Or gadEvt.i = #PB_EventType_RightClick
      ; make the canvas the active gadget
      SetActiveGadget(*tree\gadgetId)
    EndIf
   
    ; redraw tree
    redraw(*tree)
   
  EndProcedure
  
  Procedure.i resizeTree(*treeId.ct_main, x.i, y.i, width.i, height.i)
    
    With *treeId
      
      If x.i = \x And y.i = \y And width.i = \width And height.i = \height
        ; Do nothing if size has not changed!
        ; Do not all the work and redraw in this case.
        ProcedureReturn
      EndIf
    
      \x = x.i
      \y = y.i
      \width = width.i
      \height = height.i
      ResizeGadget(\gadgetId, 
                   x.i, 
                   y.i, 
                   width.i - #tree_fx_scrollBarSize,
                   height.i - #tree_fx_scrollBarSize)
      ResizeGadget(\vScrollId,
                   x.i + width.i - #tree_fx_scrollBarSize,
                   y.i,
                   #tree_fx_scrollBarSize,
                   height.i - #tree_fx_scrollBarSize)
      ResizeGadget(\hScrollId,
                   x.i,
                   y.i + height.i - #tree_fx_scrollBarSize,
                   width.i - #tree_fx_scrollBarSize,
                   #tree_fx_scrollBarSize)
      redraw(*treeId)

    EndWith
    
  EndProcedure
  
  ; create a new custom tree gadget
  Procedure.i create(x.i, y.i, width.i, height.i, flags.i = 0)
    Protected treeId.i = AddElement(cust_trees())
    Protected c.i
    ; Prepare tree and fill tree structure with some working defaults
    With cust_trees()
      \x = x.i
      \y = y.i
      \width = width.i
      \height = height.i
      \scrollX = 0
      \scrollY = 0
      \flags = flags.i
      \customization\backgroundColor = RGB(255,255,255)
      \customization\foregroundColor = RGB(60,60,60)
      \customization\selBackgroundColor = \customization\foregroundColor
      \customization\selForegroundColor = \customization\backgroundColor
      \customization\headerForegroundColor = \customization\foregroundColor
      \customization\headerBackroundColor = RGB(230,230,230)
      \customization\lineColor = RGB(180,180,180)
      \lineHeight = 20 ; some default
      \gadgetId = CanvasGadget(#PB_Any,
                               x.i,
                               y.i,
                               width.i - #tree_fx_scrollBarSize,
                               height.i - #tree_fx_scrollBarSize,
                               #PB_Canvas_Border | #PB_Canvas_Keyboard)
      \vScrollId = ScrollBarGadget(#PB_Any,
                                   x.i + width.i - #tree_fx_scrollBarSize,
                                   y.i,
                                   #tree_fx_scrollBarSize,
                                   height.i - #tree_fx_scrollBarSize,
                                   0, 1,
                                   1,
                                   #PB_ScrollBar_Vertical)
      \hScrollId = ScrollBarGadget(#PB_Any,
                                   x.i,
                                   y.i + height.i - #tree_fx_scrollBarSize,
                                   width.i - #tree_fx_scrollBarSize,
                                   #tree_fx_scrollBarSize,
                                   0, 1,
                                   1)
      ; set some default headers
      For c.i = 0 To ArraySize(\header())
        \header(c.i)\text = "Column " + Str(c.i)
        \header(c.i)\position = c.i * #tree_fx_defaultColWidth
        \header(c.i)\width = #tree_fx_defaultColWidth
      Next
      \header(0)\text = "" ; initial tree part
     
      ; bind gadget events
      BindGadgetEvent(\gadgetId,  @_cust_tree_eventHandler(),       #PB_All) ; Canvas
      BindGadgetEvent(\hScrollId, @_cust_tree_scrollEventHandler(), #PB_All) ; Scroll X
      BindGadgetEvent(\vScrollId, @_cust_tree_scrollEventHandler(), #PB_All) ; Scroll Y
    EndWith
    ProcedureReturn treeId.i
  EndProcedure
  
  ; customize the given custom tree gadget
  Procedure.i customize(*tree.ct_main, *config.ct_customizing)
    With *tree\customization
      If *config\fontId <> #PB_Ignore
        \fontId = *config\fontId
        *tree\lineHeight = _cust_tree_getLineHeight(*config\fontId)
      EndIf
      If *config\foregroundColor <> #PB_Ignore
        \foregroundColor = *config\foregroundColor
      EndIf
      If *config\backgroundColor <> #PB_Ignore
        \backgroundColor = *config\backgroundColor
      EndIf
      If *config\selForegroundColor <> #PB_Ignore
        \selForegroundColor = *config\backgroundColor
      EndIf
      If *config\selBackgroundColor <> #PB_Ignore
        \selBackgroundColor = *config\foregroundColor
      EndIf
      If *config\selForegroundColor <> #PB_Ignore
        \selForegroundColor = *config\selForegroundColor
      EndIf
      If *config\selBackgroundColor <> #PB_Ignore
        \selBackgroundColor = *config\selBackgroundColor
      EndIf
      If *config\headerForegroundColor <> #PB_Ignore
        \headerForegroundColor = *config\headerForegroundColor
      EndIf
      If *config\headerBackroundColor <> #PB_Ignore
        \headerBackroundColor = *config\headerBackroundColor
      EndIf
       If *config\lineColor <> #PB_Ignore 
        \lineColor = *config\lineColor
      EndIf
    EndWith
  EndProcedure
  
  ; add a new item to custom tree gadget
  Procedure.i addItem(*treeId.ct_main, *parent.ct_entry, text.s, imageId.i = 0, itemData.s = "")
    Protected x.i
    Protected *originalParent = *parent
    If *parent = #parent_root
      *parent = AddElement(*treeId\childs())
    Else
      *parent = AddElement(*parent\childs())
    EndIf
    *parent\status   = #open ; initial open
    *parent\imageId  = imageId.i
    *parent\itemDataStr = itemData.s
    *parent\parent   = *originalParent
    *parent\entry    = *parent ;- И как это понимать?
    If FindString(text.s, #TAB$) < 1; Or 1=1
      ; only simple entry
      *parent\text  = text.s
    Else
      ; additional text
      *parent\text  = StringField(text.s, 1, #TAB$)
      Protected headCnt.i = CountString(text.s, #TAB$) + 1
      For x.i = 2 To headCnt.i
        AddElement(*parent\columns())
        *parent\columns()\text = StringField(text.s, x.i, #TAB$)
      Next
      If *treeId\headerCount < headCnt.i - 1: *treeId\headerCount = headCnt.i - 1: EndIf
    EndIf
    ProcedureReturn *parent
  EndProcedure
  
  ; removes all selection flags for the given entry and all childs
  Procedure _unselectAll(List tree.ct_entry())
    ForEach tree()
      tree()\selected = #False
      If ListSize(tree()\childs()) > 0
        _unselectAll(tree()\childs())
      EndIf
    Next
  EndProcedure
  
  ; internal sort function (recursive)
  Procedure.i _cust_tree_sort(List tree.ct_entry(), Mode.i)
    SortStructuredList(tree(), Mode.i, OffsetOf(ct_entry\text), #PB_String)
    ForEach tree()
      If ListSize(tree()\childs()) > 0
        _cust_tree_sort(tree()\childs(), Mode.i)
      EndIf
    Next
  EndProcedure
  
  ; sort all entries in the threeview incl. redraw
  Procedure sort(*treeId.ct_main, Mode.i = #PB_Sort_Ascending | #PB_Sort_NoCase)
    _cust_tree_sort(*treeId\childs(), Mode.i)
    redraw(*treeId)
  EndProcedure
  
  ; internal column sort function (recursive)
  Procedure.i _cust_tree_column_sort_prepare(List tree.ct_entry(), columnId.i)
    If SelectElement(tree()\columns(), columnId.i)
      tree()\sortValue = tree()\columns()\text
    Else
      tree()\sortValue = "nö"
    EndIf

    ForEach tree()
      If SelectElement(tree()\columns(), columnId.i)
        tree()\sortValue = tree()\columns()\text
      Else
        tree()\sortValue = "nö"
      EndIf
      If ListSize(tree()\childs()) > 0
        _cust_tree_column_sort_prepare(tree()\childs(), columnId.i)
      EndIf
    Next
  EndProcedure
  
  ; internal column sort function (recursive)
  Procedure.i _cust_tree_column_sort(List tree.ct_entry(), Mode.i)
    SortStructuredList(tree(), Mode.i, OffsetOf(ct_entry\sortValue), #PB_String)
    ForEach tree()
      If ListSize(tree()\childs()) > 0
        _cust_tree_column_sort(tree()\childs(), Mode.i)
      EndIf
    Next
  EndProcedure
  
  ; sort all entries in the threeview by column incl. redraw
  Procedure sortColumn(*treeId.ct_main, columnId.i, Mode.i = #PB_Sort_Ascending | #PB_Sort_NoCase)
    If columnId.i < 0
      ; no sort
      ProcedureReturn
    EndIf
    If columnId.i = 0
      ; standard sort
      _cust_tree_sort(*treeId\childs(), Mode.i)
    Else
      ; column sort
      _cust_tree_column_sort_prepare(*treeId\childs(), columnId.i - 1)
      _cust_tree_column_sort(*treeId\childs(), Mode.i)
    EndIf
    redraw(*treeId)
  EndProcedure
  
  ; internal search function (recursive)
  Procedure.i _cust_tree_getSelected(List tree.ct_entry(), List entries.ct_entry())
    ForEach tree()
      If tree()\selected = #True
        AddElement(entries())
        entries() = tree()
      EndIf
      If ListSize(tree()\childs()) > 0
        _cust_tree_getSelected(tree()\childs(), entries())
      EndIf
    Next
  EndProcedure
  
  ; returns all selected entried in given entries LinkedList of type ct_entry.
  Procedure.i getSelected(*treeId.ct_main, List entries.ct_entry())
    ClearList(entries())
    _cust_tree_getSelected(*treeId\childs(), entries())
  EndProcedure
  
  ; Resize a given header.
  ; You need to call redraw() in order to see the changes.
  Procedure.i resizeHeader(*treeId.ct_main, columnId.i, width.i)
    If columnId.i < 0 Or columnId.i > *treeId\headerCount
      ProcedureReturn #False
    EndIf
   
    Protected c.i
    *treeId\header(columnId.i)\width = width.i
    ; re-pos the other columns
    For c.i = columnId+1 To *treeId\headerCount
      *treeId\header(c.i)\position = *treeId\header(c.i-1)\position + *treeId\header(c.i-1)\width
    Next
    ProcedureReturn #True
  EndProcedure
  
  Procedure.i setIcon(*entry.ct_entry, columnId.i, imageId.i)
    If columnId.i < 1
      ; main entry
      *entry\imageId = imageId.i
    Else
      If SelectElement(*entry\columns(), columnId.i - 1) <> 0
        *entry\columns()\imageId = imageId.i
      Else
        Debug "Column " + Str(columnId.i) + " does not exist for entry '" + *entry\text + "'"
      EndIf
    EndIf
  EndProcedure
  
  ; returns the column Id the cursor is currently on
  ; -1 is "not over a header"
  Procedure.i mouseOnHeader(*treeId.ct_main, x.i = #PB_Ignore, y.i = #PB_Ignore)
    If x.i = #PB_Ignore
      x.i = GetGadgetAttribute(*treeId\gadgetId, #PB_Canvas_MouseX)
    EndIf
    If y.i = #PB_Ignore
      y.i = GetGadgetAttribute(*treeId\gadgetId, #PB_Canvas_MouseY)
    EndIf
    If y.i > *treeId\lineHeight Or *treeId\headerCount < 1
      ; definitely no column header!
      ProcedureReturn -1
    EndIf
    Protected p.i = 0
    Protected c.i = 0
    For c.i = 0 To *treeId\headerCount
      p.i = *treeId\header(c.i)\position + *treeId\header(c.i)\width + *treeId\scrollX
      If x.i < p.i
        ProcedureReturn c.i
      EndIf
    Next
    ProcedureReturn -1
  EndProcedure
  
   ;- Удаление всех элементов. В отличии от оригинальной clear() не убирает столбцы.
  Procedure clearAllItems(*treeId.ct_main)
    ClearList(*treeId\childs()) 
    redraw(*treeId)
  EndProcedure
  
  ;- Вернёт DataPtr текущего элемента в TreeView *treeId.
  ; При активном множественном выборе элементов (#flags_multiSelect) вернёт значение только первого 
  ; выбранного элемента.
  Procedure.s getCurrentItemDataStr(*treeId.ct_main)
    NewList entries.ct_entry()
    getSelected(*treeId, entries())
    If ListSize(entries()) > 0
      ProcedureReturn entries()\itemDataStr
    Else
      ProcedureReturn "-1"
    EndIf
  EndProcedure
  
  ;- Вернёт parent текущего элемента в TreeView *treeId.
  ; При активном множественном выборе элементов (#flags_multiSelect) вернёт значение только первого 
  ; выбранного элемента.
  ;- Теперь не работает!
  Procedure getCurrentEntry(*treeId.ct_main)
    NewList entries.ct_entry()
    getSelected(*treeId, entries())
    If ListSize(entries()) > 0
      ProcedureReturn entries()\entry
    Else
      ProcedureReturn 0
    EndIf
  EndProcedure
  
  ;- ВНЕЗАПНО и рекурсивно проставит tree()\selected = #False кроме *entry.
  ; Проще говоря отметит теущей строку *entry.
  Procedure.i _cust_tree_setSelected(List tree.ct_entry(), *entry.ct_entry)
    _cust_tree_reset_selection(tree.ct_entry())
    ForEach tree()
      If tree()\entry = *entry
        tree()\selected = #True
      Else
        tree()\selected = #False
      EndIf
      If ListSize(tree()\childs()) > 0
        _cust_tree_setSelected(tree()\childs(), *entry.ct_entry)
      EndIf
    Next
  EndProcedure
  
  ;- ВНЕЗАПНО и рекурсивно установит эту иконку всем строкам в списке.
  Procedure.i _cust_set_icon_all_items(List tree.ct_entry(), columnId.i, imageId.i)
    If columnId.i < 1
      ; main entry
      ForEach tree()
        tree()\imageId = imageId.i
        If ListSize(tree()\childs()) > 0
          _cust_set_icon_all_items(tree()\childs(), columnId.i, imageId.i)
        EndIf
      Next
    Else
      ForEach tree()
        If SelectElement(tree()\columns(), columnId.i - 1) <> 0
          tree()\columns()\imageId = imageId.i
        Else
          Debug "Column " + Str(columnId.i) + " does not exist for entry '" + tree()\text + "'"
        EndIf
        If ListSize(tree()\childs()) > 0
          _cust_set_icon_all_items(tree()\childs(), columnId.i, imageId.i)
        EndIf
      Next
    EndIf
  EndProcedure
  
  ;- Установит эту иконку всем строкам в списке.
  Procedure.i setIconAllItems(*treeId.ct_main, columnId.i, imageId.i)
    _cust_set_icon_all_items(*treeId\childs(), columnId.i, imageId.i)
    redraw(*treeId)
  EndProcedure
  
  ;- Сделает элемент *entry активным в *treeId.
  Procedure.i setState(*treeId.ct_main, *entry.ct_entry)
    _cust_tree_setSelected(*treeId\childs(), *entry.ct_entry)
    ;If (*entry\y = -1)
    ;  *treeId\scrollY + 100
    ;EndIf
    redraw(*treeId)
  EndProcedure
  
EndModule
DisableExplicit
; IDE Options = PureBasic 5.31 (Windows - x86)
; CursorPosition = 1236
; FirstLine = 1211
; Folding = -------
; EnableUnicode
; EnableXP