﻿
DeclareModule FTN
  
  Structure TplVariables
    from_name.s
    from_ftn_addr.s
    to_name.s
    to_ftn_addr.s
    reply_to_name.s
    reply_to_ftn_addr.s
    quoted_text.s
    reply_date.q
  EndStructure
  
  Enumeration MsgType
    #New
    #Reaply
  EndEnumeration

  Declare.s LoadTemplate(*msg.TplVariables, tpl_name.s)
  Declare.s CreateQuotes(author.s , text.s)
  Declare.s GetSignatures()
  Declare.s FixText(text.s)

EndDeclareModule

Module FTN
  
  ; Перенос строк по максимальной длине (maxsize).
  Procedure.s WordWrap(maxsize, text.s)
    size = Len(text.s) ; Получаем длину строки (размер массива текста).
    counter=1
    For symbol_number = 1 To size; Цикл, %номер_символа% в котором перебираем значения от 1 до %длина строки%.
      symbol.s = Mid(text.s, symbol_number, 1) ; Получаем значение символа под номером symbol_number.
      If symbol.s=Chr(10) ; Если символ перенос строки, то %счётчик%=1.
        counter=0
      EndIf
      If symbol.s=" " ; Если пробел, то сохраняем %номер_символа% в %последний_пробел%.
        last_gap=symbol_number
      EndIf
      If counter>maxsize ; Если %счётчик% больше %ограничение строки%, то заменяем %последний_пробел% на перенос.
        If last_gap=0 ; Если %последний_пробел%=0, то вклиниваем перенос строки после %ограничение строки%.
          text.s=InsertString(text.s, Chr(10), symbol_number)
          size=size+1
          counter=0
        Else
          text.s=InsertString(text.s, Chr(10), last_gap+1)
          size = Len(text.s) ; Обновляем длину строки (размер массива текста).
          counter=0
        EndIf
      EndIf
      counter=counter+1
    Next
    ProcedureReturn text.s
  EndProcedure
  
  ; Возращает цитированный текст, созданный из text.s.
  ; Инициалы для квотинга берутся из author.s.
  ;- Надо перепилить, так как не соответствует стандартуъ!
  Procedure.s CreateQuotes(author.s , text.s)

    ; Получаем строчку начала авторской квоты.
    StartPosition=1
    For i=1 To CountString(author.s, " ")+1
      result.s=result.s+Mid(author.s, StartPosition, 1)
      StartPosition = FindString(author.s, " ", StartPosition)+1
    Next
    
    author.s=" "+result.s+"> "
    text.s = WordWrap(72, text.s)
    
    ; Построчно добавляем авторскую надбавку в случае, если в строчке нет квоты, если есть то просто добавляем ">".
    numbersStrings=CountString(text.s, Chr(10)) ; По переносам определяем число строк.
    For string=1 To numbersStrings ; Перебираем в цикле номера строк.
      StartString=EndString
      EndString=FindString(text.s, Chr(10), StartString+1)
      TwoPoint=FindString(text.s, "> ", StartString+1) ; Получаем расположение 1 пробела в строке.
      StringCitation.s=Trim(Mid(text.s, StartString+1, EndString-StartString))
      If CountString(StringCitation.s, "> ")<>0
        If CountString(Trim(Mid(text.s, StartString+2, TwoPoint-StartString)), " ")<1 ; Обределяем наличие квоты.
          text.s=InsertString(text.s, ">", TwoPoint)
          EndString=EndString+1
        Else
          If EndString-StartString>2
            text.s=InsertString(text.s, author.s, StartString+1)
            EndString=EndString+Len(author.s)
          EndIf
        EndIf
      Else
        If EndString-StartString>2
          text.s=InsertString(text.s, author.s, StartString+1)
          EndString=EndString+Len(author.s)
        EndIf
      EndIf
    Next
    
    ProcedureReturn text.s
  EndProcedure
  
  ; Вернёт шаблон сообщения.
  Procedure.s LoadTemplate(*msg.TplVariables, tpl_name.s)
    
    ; Собственно, считываем сам шаблон.
    file_id = ReadFile(#PB_Any, "templates/"+tpl_name.s+".tpl")
    If file_id
      While Eof(file_id) = 0
        template.s + ReadString(file_id) + Chr(10)
      Wend
      CloseFile(file_id)
    Else
      ;- Шаблон не существует - нужно создать его!
      MessageRequester("Information","Couldn't open the file!")
    EndIf
    
    ;{ Заменяем переменные в соответствии с информацией из *msg.DB::Message и не только :Р.
    
    template.s = ReplaceString(template.s, "@FName", *msg\from_name, #PB_String_NoCase)
    template.s = ReplaceString(template.s, "@TName", *msg\to_name, #PB_String_NoCase)
    template.s = ReplaceString(template.s, "@OAddr", *msg\to_ftn_addr, #PB_String_NoCase)
    template.s = ReplaceString(template.s, "@DName", *msg\reply_to_name, #PB_String_NoCase)
    template.s = ReplaceString(template.s, "@DAddr", *msg\reply_to_ftn_addr, #PB_String_NoCase)
    template.s = ReplaceString(template.s, "@ODate", Date::CreateDateString(*msg\reply_date), #PB_String_NoCase)
    template.s = ReplaceString(template.s, "@Quoted@Position", CreateQuotes(*msg\to_name, *msg\quoted_text), #PB_String_NoCase)

    ;}
    
    ProcedureReturn template.s
  EndProcedure 
  
  ; Вернёт подписи (ориджин, теарлайн и теглайн).
  Procedure.s GetSignatures()
    Protected tagline.s, tearline.s, origin.s, result.s
    
    tagline.s = "... "+CFG::Signatures\tagline+Chr(13)
    tearline.s = "--- "+CFG::Signatures\tearline+Chr(13)
    origin.s = " * Origin: "+CFG::Signatures\origin+" ("+CFG::User\Address+")"
    
    If Not Len(tagline.s)<=6
      result.s + tagline.s
    EndIf
    
    If Not Len(tearline.s)<=6
      result.s + tearline.s
    EndIf
    
    result.s + origin.s
    
    ProcedureReturn result.s
  EndProcedure
  
  ; Исправит 'SOH' на '@'.
  Procedure.s FixText(text.s)
    Protected result.s
    
    result.s = ReplaceString(text.s, Chr(1), "@")
    
    ProcedureReturn result.s
  EndProcedure
  
EndModule

; IDE Options = PureBasic 5.31 (Windows - x86)
; CursorPosition = 22
; Folding = --
; EnableUnicode
; EnableXP