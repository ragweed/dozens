﻿DeclareModule Kludges
  
  Declare.s SearchReplyKludge(text.s)
  Declare.s SearchMsgidKludge(text.s)
  
  Declare.s CreateKludgeString(control_tag.s, string.s)
  Declare.s CreateMsgidKludge(from_ftn_adrr.s)
  Declare.s CreateMsgidString(from_ftn_adrr.s)
  Declare.s CreateReplyKludge(string.s)
  Declare.s GetPidKludge()
  Declare.s GetChrsKludge()
  Declare.s GetTzutcKludge()
  
EndDeclareModule  

Module Kludges
  
  ; Ищет и возращает в случае успеха кладж Reply.
  Procedure.s  SearchReplyKludge(text.s)
    If CountString(text.s, Chr(01)+"REPLY:")<>0 ; Провераем наличие нужного кладжа в тексте сообщения.
      StartPosition = FindString(text.s, Chr(01)+"REPLY:", 1) ; Получаем индекс начало кладжа в тексте.
      Spase = FindString(text.s, " ", StartPosition+9)
      EndPosition=Spase+8
      size=EndPosition-(StartPosition+7);EndPosition-(StartPosition+8)
      test.s=Mid(text.s, StartPosition+8, size) ; Извлекаем кладж из текста.
      ProcedureReturn test.s
    Else
      ProcedureReturn ""       
    EndIf
  EndProcedure
  
  ; Ищет и возращает в случае успеха кладж Msgid.
  Procedure.s  SearchMsgidKludge(text.s)
    If CountString(text.s, Chr(01)+"MSGID:")<>0 ; Провераем наличие нужного кладжа в тексте сообщения.
      StartPosition = FindString(text.s, Chr(01)+"MSGID:", 1) ; Получаем индекс начало кладжа в тексте.
      Spase = FindString(text.s, " ", StartPosition+9)
      EndPosition=Spase+8
      size=EndPosition-(StartPosition+7);EndPosition-(StartPosition+8)
      test.s=Mid(text.s, StartPosition+8, size) ; Извлекаем кладж из текста.
      ProcedureReturn test.s
    Else
      ProcedureReturn ""       
    EndIf
  EndProcedure
  
  ; Создаёт кладж.
  Procedure.s CreateKludgeString(control_tag.s, string.s)
    ProcedureReturn Chr(1)+control_tag.s+": "+string.s+Chr(13)
  EndProcedure
  
  ; Создаёт уникальную строку для кладжа Msgid.
  Procedure.s CreateMsgidString(from_ftn_adrr.s)
    ;- А что делать будем с дублирующимися кладжами?
    date.s = LCase(RSet(Hex(Date::UTCTime(), #PB_Long), 8, "0"))
    ProcedureReturn from_ftn_adrr.s+" "+date.s
  EndProcedure
  
  ; Создаёт кладж Msgid.
  Procedure.s CreateMsgidKludge(from_ftn_adrr.s)
    ProcedureReturn CreateKludgeString("MSGID", CreateMsgidString(from_ftn_adrr.s))
  EndProcedure
  
  ; Создаёт кладж Reply.
  Procedure.s CreateReplyKludge(string.s)
    ProcedureReturn CreateKludgeString("REPLY", string.s)
  EndProcedure
  
  ; Вернёт кладж Pid.
  Procedure.s GetPidKludge()
    CompilerSelect #PB_Compiler_OS
      CompilerCase #PB_OS_Windows
        ProcedureReturn CreateKludgeString("PID", "Dozens/Windows")
      CompilerCase #PB_OS_Linux
        ProcedureReturn CreateKludgeString("PID", "Dozens/Linux")
    CompilerEndSelect
  EndProcedure 
  
  ; Вернёт кладж Chrs.
  Procedure.s GetChrsKludge()
    ;- Может значение CHRS хранить в конфиге?
    ProcedureReturn CreateKludgeString("CHRS", "CP866 2")
  EndProcedure 
  
  ; Вернёт кладж Tzutc.
  Procedure.s GetTzutcKludge()
    ProcedureReturn CreateKludgeString("TZUTC", Date::GetTimeBias())
  EndProcedure 

EndModule

; IDE Options = PureBasic 5.30 (Windows - x86)
; CursorPosition = 79
; FirstLine = 56
; Folding = --
; EnableUnicode
; EnableXP